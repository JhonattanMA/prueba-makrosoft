<?php

namespace App\Services\Configuracion\Implementations;

use App\Models\Configuracion\Rol;
use App\Services\Configuracion\PermisoService;
use App\Services\Configuracion\RolService;

class RolServiceImpl implements RolService
{
    private $permisoService;

    public function __construct(PermisoService $permisoService)
    {
        $this->permisoService = $permisoService;
    }

    /**
     * Funcion para obtener todos los roles
     * @return object Coleccion con todos los roles
     * @author Jhonatan Mariaca
     * @fecha 14-06-2024
     */
    public function obtenerRoles():object
    {
        return Rol::orderBy('name','ASC');
    }

    /**
     * Funcion para obtener el rol mediante el id
     * @param int $rolId, Id del rol
     * @return object coleccion con el rol
     * @author Jhonatan mariaca
     * @fecha 14-06-2024
     */
    public function obtenerRol(int $rolId):?object
    {
        return Rol::find($rolId);
    }

    /**
     * Funcion para obtener el rol mediante el nombre
     * @param string $nombre, Nombre del rol
     * @return object coleccion con el rol
     * @author Jhonatan mariaca
     * @fecha 14-06-2024
     */
    public function obtenerRolPorNombre(string $nombre):? object
    {
        return Rol::where('name', $nombre)->first();
    }

    /**
     * Funcion para crear un rol
     * @param string $nombre, Nombre del rol
     * @return bool Retorna true o false
     * @author Jhonatan mariaca
     * @fecha 14-06-2024
     */
    public function registrarRol(string $nombre):bool
    {
        $rol = new Rol();
        $rol->name = $nombre;
        return $rol->save();
    }

    /**
     * Funcion para actualizar un rol
     * @param int $rolId, Id del rol
     * @param string $nombre, Nombre del rol
     * @return bool Retorna true o false
     * @author Jhonatan mariaca
     * @fecha 14-06-2024
     */
    public function actualizarRol(int $rolId, string $nombre):bool
    {
        $rol = Rol::where('id',$rolId)->update([
            'name' => $nombre
        ]);

        return $rol;
    }

    /**
     * Funcion para eliminar un rol
     * @param int $rolId, Id del rol
     * @return bool Retorna true o false
     * @author Jhonatan Mariaca
     * @fecha 14-06-2024
     */
    public function eliminarRol(int $rolId):bool
    {
        return Rol::destroy($rolId);
    }

    /**
     * Funcion para asignarle permisos al rol
     * @param int $rolId, Id del rol
     * @param array $permisos, arreglo con los permisos
     * @return bool Retorna true o false
     * @author Jhonatan Mariaca
     * @fecha 14-06-2024
     */
    public function asignarPermisosARol(int $rolId, array $permisos):bool
    {
        $rol = $this->obtenerRol($rolId);
        $permisos = $this->permisoService->obtenerPermisos()->whereIn('id',$permisos)->get();
        $respuesta = $rol->syncPermissions($permisos);;

        if (!$respuesta) {
            return false;
        }

        return true;
    }
}
