<?php

namespace App\Services\Configuracion\Implementations;

use App\Models\Configuracion\Permiso;
use App\Services\Configuracion\PermisoService;

class PermisoServiceImpl implements PermisoService
{
    /**
     * Funcion para obtener todos los permisos
     * @return object Coleccion con todos los permisos
     * @author Jhonatan Mariaca
     * @fecha 14-06-2024
     */
    public function obtenerPermisos(): object
    {
        return Permiso::orderBy('name','ASC');
    }

    /**
     * Funcion para obtener el permiso mediante el id
     * @param int $permisoId, Id del permiso
     * @return object coleccion con el permiso
     * @author Jhonatan mariaca
     * @fecha 14-06-2024
     */
    public function obtenerPermiso(int $permisoId):object
    {
        return Permiso::find($permisoId);
    }

    /**
     * Funcion para obtener el permiso mediante el nombre
     * @param string $nombre, Nombre del permiso
     * @return object coleccion con el permiso
     * @author Jhonatan mariaca
     * @fecha 14-06-2024
     */
    public function obtenerPermisoPorNombre(string $nombre):?object
    {
        return Permiso::where('name', $nombre)->first();
    }

    /**
     * Funcion para crear un permiso
     * @param string $nombre, Nombre del permiso
     * @return bool Retorna true o false
     * @author Jhonatan mariaca
     * @fecha 14-06-2024
     */
    public function registrarPermiso(string $nombre):bool
    {
        $permiso = new Permiso();
        $permiso->name = $nombre;
        return $permiso->save();
    }

    /**
     * Funcion para actualizar un permiso
     * @param int $permisoId, Id del permiso
     * @param string $nombre, Nombre del permiso
     * @return bool Retorna true o false
     * @author Jhonatan mariaca
     * @fecha 14-06-2024
     */
    public function actualizarPermiso(int $permisoId, string $nombre):bool
    {
        $permiso = Permiso::where('id',$permisoId)->update([
            'name' => $nombre
        ]);

        return $permiso;
    }

    /**
     * Funcion para eliminar un permiso
     * @param int $permisoId, Id del permiso
     * @return bool Retorna true o false
     * @author Jhonatan Mariaca
     * @fecha 14-06-2024
     */
    public function eliminarPermiso(int $permisoId):bool
    {
        return Permiso::destroy($permisoId);
    }
}
