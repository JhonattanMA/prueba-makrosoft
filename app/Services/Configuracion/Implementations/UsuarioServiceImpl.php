<?php

namespace App\Services\Configuracion\Implementations;

use App\Models\User;
use App\Services\Configuracion\PermisoService;
use App\Services\Configuracion\UsuarioService;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;

class UsuarioServiceImpl implements UsuarioService
{
    private $permisoService;

    public function __construct(PermisoService $permisoService)
    {
        $this->permisoService = $permisoService;
    }

    /**
     * Funcion para obtener todos los usuarios
     * @return object Coleccion con todos los usuarios
     * @author Jhonatan Mariaca
     * @fecha 14-06-2024
     */
    public function obtenerUsuarios():object
    {
        return User::orderBy('nombre_completo','ASC');
    }

    /**
     * Funcion para obtener el usuario por Id
     * @param int $usuarioId, Id del usuario
     * @return object Coleccion con el usuario
     * @author Jhonatan Mariaca
     * @fecha 14-06-2024
     */
    public function obtenerUsuario(int $usuarioId):?object
    {
        return User::find($usuarioId);
    }

    /**
     * Funcion para asignar el rol al usuario
     * @param int $usuarioId, Id del usuario
     * @param int $rolId, Id del rol
     * @author Jhonatan Mariaca
     * @fecha 14-06-2024
     */
    public function asignarRolAUsuario(int $usuarioId, int $rolId):bool
    {
        $usuario = $this->obtenerUsuario($usuarioId);
        $respuesta = $usuario->assignRole($rolId);

        if (!$respuesta) {
            return false;
        }

        return true;
    }

    /**
     * Funcion para remover el rol al usuario
     * @param int $usuarioId, Id del usuario
     * @param int $rolId, Id del rol
     * @author Jhonatan Mariaca
     * @fecha 14-06-2024
     */
    public function removerRolAUsuario(int $usuarioId, int $rolId):bool
    {
        $usuario = $this->obtenerUsuario($usuarioId);
        $respuesta = $usuario->removeRole($rolId);

        if (!$respuesta) {
            return false;
        }

        return true;
    }

    /**
     * Funcion para asignar permisos al usuario
     * @param int $usuarioId, Id del usuario
     * @param int $permisos, Array de los permisos
     * @author Jhonatan Mariaca
     * @fecha 14-06-2024
     */
    public function asignarPermisosAUsuario(int $usuarioId, array $permisos):bool
    {
        $usuario = $this->obtenerUsuario($usuarioId);
        $permisos = $this->permisoService->obtenerPermisos()->whereIn('id',$permisos)->get();
        $respuesta = $usuario->syncPermissions($permisos);

        if (!$respuesta) {
            return false;
        }

        return true;
    }

    /**
     *
     * Funcion para registrar un usuario
     *
     * @author Jhonatan Mariaca
     * @date 10-11-2023
     * @param array: arreglo con los datos a registrar
     * @return int|bool: id del registro en caso de registrar correctamente
     *              falso en caso contrario
     */
    public function resgistrarUsuario(array $datos): object|bool
    {
        $respuesta = User::create($datos);

        if (!$respuesta) {
            return false;
        }

        return $respuesta;
    }

    /**
     *
     * Funcion para actualizar un usuario
     *
     * @author Jhonatan Mariaca
     * @date 10-11-2023
     * @param int: id
     * @param array: arreglo con los datos a actualizar
     * @return bool: verdadero en caso de actualizar correctamente
     *              falso en caso contrario
     */
    public function actualizarUsuario(int $id, array $datos): bool
    {
        $respuesta = User::where('id',$id)->update($datos);

        if (!$respuesta) {
            return false;
        }

        return true;

    }
}
