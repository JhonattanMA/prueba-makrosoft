<?php

namespace App\Services\Configuracion;

interface PermisoService
{
    public function obtenerPermisos():object;
    public function obtenerPermiso(int $permisoId):object;
    public function obtenerPermisoPorNombre(string $nombre):?object;
    public function registrarPermiso(string $nombre):bool;
    public function actualizarPermiso(int $permisoId, string $nombre):bool;
    public function eliminarPermiso(int $permisoId):bool;
}
