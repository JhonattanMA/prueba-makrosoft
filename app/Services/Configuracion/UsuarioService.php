<?php

namespace App\Services\Configuracion;

interface UsuarioService
{
    public function obtenerUsuarios():object;
    public function obtenerUsuario(int $usuarioId):?object;
    public function asignarRolAUsuario(int $usuarioId, int $rolId):bool;
    public function removerRolAUsuario(int $usuarioId, int $rolId):bool;
    public function asignarPermisosAUsuario(int $usuarioId, array $permisos):bool;
    public function resgistrarUsuario(array $datos): object|bool;
    public function actualizarUsuario(int $id, array $datos): bool;
}
