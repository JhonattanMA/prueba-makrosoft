<?php

namespace App\Services\Configuracion;

interface RolService
{
    public function obtenerRoles():object;
    public function obtenerRol(int $permisoId):?object;
    public function obtenerRolPorNombre(string $nombre):?object;
    public function registrarRol(string $nombre):bool;
    public function actualizarRol(int $permisoId, string $nombre):bool;
    public function eliminarRol(int $rolId):bool;
    public function asignarPermisosARol(int $rolId, array $permisos):bool;
}
