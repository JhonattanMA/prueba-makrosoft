<?php

namespace App\Services\Trabajadores\Implementations;

use App\Models\Trabajadores\PesoAcumulado;
use App\Services\Trabajadores\PesoAcumuladoService;

class PesoAcumuladoServiceImpl implements PesoAcumuladoService
{
/**
     * Funcion para registar el peso acumulado
     * @param int $pesoAcumuladoId, id del peso acumulado
     * @param array $datos, array con los datos a registrar del peso acumulado
     * @return bool retorna un boolean true o false
     * @author Jhonatan Mariaca
     * @fecha 14-06-2024
     */
    public function registrarPesoAcumulado(int $pesoAcumuladoId, array $datos):bool
    {
        $respuesta = PesoAcumulado::where('id',$pesoAcumuladoId)->update($datos);

        if (!$respuesta) {
            return false;
        }

        return true;
    }
}
