<?php

namespace App\Services\Trabajadores\Implementations;

use App\Models\Trabajadores\Trabajador;
use App\Services\Trabajadores\TrabajadorService;

class TrabajadorServiceImpl implements TrabajadorService
{
    /**
     * Funcion para obtener todos los trabajadors
     * @return object Coleccion con todos los trabajadors
     * @author Jhonatan Mariaca
     * @fecha 14-06-2024
     */
    public function obtenerTrabajadores():object
    {
        return Trabajador::orderBy('nombres','ASC');
    }

    /**
     * Funcion para obtener el trabajador mediante el id
     * @param int $trabajadorId, Id del trabajador
     * @return object coleccion con el trabajador
     * @author Jhonatan mariaca
     * @fecha 14-06-2024
     */
    public function obtenerTrabajador(int $trabajadorId):object
    {
        return Trabajador::find($trabajadorId);
    }

    /**
     * Funcion para obtener el trabajador con el peso acumulado menor
     * @return object coleccion con el trabajador
     * @author Jhonatan mariaca
     * @fecha 14-06-2024
     */
    public function obtenerTrabajadorAcumuladoMenor():object
    {
        return Trabajador::join('pesos_acumulados','trabajadores.id','pesos_acumulados.trabajadores_id')
                            ->selectRaw("trabajadores.id, MIN(pesos_acumulados.peso_acumulado) as peso_acumulado, pesos_acumulados.id as peso_acumulado_id")
                            ->where('pesos_acumulados.peso_acumulado','peso_acumulado')
                            ->groupby('trabajadores.id','pesos_acumulados.id')
                            ->first();
    }

    /**
     * Funcion para crear un trabajador
     * @param array $datos, Array con los datos del trabajador
     * @return bool Retorna true o false
     * @author Jhonatan mariaca
     * @fecha 14-06-2024
     */
    public function registrarTrabajador(array $datos):bool
    {
        $respuesta = Trabajador::create($datos);

        if (!$respuesta) {
            return false;
        }

        return true;
    }

    /**
     * Funcion para actualizar un trabajador
     * @param int $trabajadorId, Id del trabajador
     * @param array $datos, Array con los datos del trabajador
     * @return bool Retorna true o false
     * @author Jhonatan mariaca
     * @fecha 14-06-2024
     */
    public function actualizarTrabajador(int $trabajadorId, array $datos):bool
    {
        $respuesta = Trabajador::where('id',$trabajadorId)->update($datos);

        if (!$respuesta) {
            return false;
        }

        return true;
    }

    /**
     * Funcion para eliminar un trabajador
     * @param int $trabajadorId, Id del trabajador
     * @return bool Retorna true o false
     * @author Jhonatan Mariaca
     * @fecha 14-06-2024
     */
    public function eliminarTrabajador(int $trabajadorId):bool
    {
        return Trabajador::destroy($trabajadorId);
    }
}
