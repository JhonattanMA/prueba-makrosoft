<?php

namespace App\Services\Trabajadores;

interface TrabajadorService
{
    public function obtenerTrabajadores():object;
    public function obtenerTrabajador(int $trabajadorId):object;
    public function obtenerTrabajadorAcumuladoMenor():object;
    public function registrarTrabajador(array $datos):bool;
    public function actualizarTrabajador(int $trabajadorId, array $datos):bool;
    public function eliminarTrabajador(int $trabajadorId):bool;
}
