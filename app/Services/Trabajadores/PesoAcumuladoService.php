<?php

namespace App\Services\Trabajadores;

interface PesoAcumuladoService
{
    public function registrarPesoAcumulado(int $pesoAcumuladoId, array $datos):bool;
}
