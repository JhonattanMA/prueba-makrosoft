<?php

namespace App\Services\Soportes;

interface SoporteService
{
    public function obtenerSoportes():object;
    public function obtenerSoportesSinAsignacion():object;
    public function obtenerSoporte(int $soporteId):object;
    public function registrarSoporte(array $datos):bool;
    public function actualizarSoporte(int $soporteId,array $datos):bool;
}
