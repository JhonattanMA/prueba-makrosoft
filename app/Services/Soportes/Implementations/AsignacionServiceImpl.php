<?php

namespace App\Services\Soportes\Implementations;

use App\Models\Soportes\Asignacion;
use App\Services\Soportes\AsignacionService;
use App\Services\Soportes\SoporteService;
use App\Services\Trabajadores\PesoAcumuladoService;
use App\Services\Trabajadores\TrabajadorService;

class AsignacionServiceImpl implements AsignacionService
{
    private $trabajadorService;
    private $soporteService;
    private $pesoAcumuladoService;

    public function __construct(TrabajadorService $trabajadorService, SoporteService $soporteService, PesoAcumuladoService $pesoAcumuladoService)
    {
        $this->trabajadorService = $trabajadorService;
        $this->soporteService = $soporteService;
        $this->pesoAcumuladoService = $pesoAcumuladoService;
    }

    /**
     * Funcion para asignar los soportes a los trabajadores
     * @author Jhonatan Mariaca
     * @fecha 14-06-2024
     */
    public function asignarSoportes()
    {
        $trabajador = $this->trabajadorService->obtenerTrabajadorAcumuladoMenor();
        $soportes = $this->soporteService->obtenerSoportesSinAsignacion();
        $pesoAcumulado = $trabajador->peso_acumulado;

        if (count($soportes) > 0) {
            foreach ($soportes as $soporte) {
                $datosAsignacion = array(
                    'fecha_asignacion' => now(),
                    'trabajadores_id' => $trabajador->id,
                    'soportes_id' => $soporte->id,
                );

                $pesoAcumulado += $soporte->peso_trabajo;

                $this->registrarAsignacion($datosAsignacion);
            }

            $datosPesoAcumulado = array(
                'fecha' => now(),
                'peso_acumulado' => $pesoAcumulado,
                'trabajadores_id' => $trabajador->id,
            );
            $respuesta = $this->pesoAcumuladoService->registrarPesoAcumulado($trabajador->peso_acumulado_id, $datosPesoAcumulado);

            if (!$respuesta) {
                return false;
            }

            return true;
        }

        return 0;

    }

    /**
     * Funcion para registar las asignaciones
     * @param array $datos, array con los datos a registrar de la asignacion
     * @return bool retorna un boolean true o false
     * @author Jhonatan Mariaca
     * @fecha 14-06-2024
     */
    public function registrarAsignacion(array $datos):bool
    {
        $respuesta = Asignacion::create($datos);

        if (!$respuesta) {
            return false;
        }

        return true;
    }
}
