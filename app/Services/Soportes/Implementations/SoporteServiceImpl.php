<?php

namespace App\Services\Soportes\Implementations;

use App\Models\Soportes\Soporte;
use App\Services\Soportes\SoporteService;

class SoporteServiceImpl implements SoporteService
{
    /**
     * Funcion para obtener los datos de todos los soportes
     * @return object Coleccion con todos los datos de los soportes
     * @author Jhonatan Mariaca
     * @fecha 14-06-2024
     */
    public function obtenerSoportes():object
    {
        return Soporte::orderBy('nombres', 'ASC');
    }

    /**
     * Funcion para obtener los datos de todos los soportes sin asignaciones
     * @return object Coleccion con todos los datos de los soportes
     * @author Jhonatan Mariaca
     * @fecha 14-06-2024
     */
    public function obtenerSoportesSinAsignacion():object
    {
        return Soporte::select('soportes.*')
                        ->leftjoin('asignaciones','soportes.id','asignaciones.soportes_id')
                        ->where('asignaciones.soportes_id', null)
                        ->get();
    }

    /**
     * Funcion para obtener los datos de un soporte
     * @param int $soporteId, id del soporte
     * @return object Coleccion con todos los datos de un soporte
     * @author Jhonatan Mariaca
     * @fecha 14-06-2024
     */
    public function obtenerSoporte(int $soporteId):object
    {
        return Soporte::find($soporteId);
    }

    /**
     * Funcion para registrar un soporte
     * @param array $datos, array con los datos a registrar del soporte
     * @return bool retorna un boolean true o false
     * @author Jhonatan Mariaca
     * @fecha 14-06-2024
     */
    public function registrarSoporte(array $datos):bool
    {
        $respuesta = Soporte::create($datos);

        if (!$respuesta) {
            return false;
        }

        return true;
    }

    /**
     * Funcion para actualizar un soporte
     * @param int $soporteId, id del soporte
     * @param array $datos, array con los datos a actualizar del soporte
     * @return bool retorna un boolean true o false
     * @author Jhonatan Mariaca
     * @fecha 14-06-2024
     */
    public function actualizarSoporte(int $soporteId,array $datos):bool
    {
        $respuesta = Soporte::where('id',$soporteId)->update($datos);

        if (!$respuesta) {
            return false;
        }

        return true;
    }
}
