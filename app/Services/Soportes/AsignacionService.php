<?php

namespace App\Services\Soportes;

interface AsignacionService
{
    public function registrarAsignacion(array $datos);
}
