<?php

use Carbon\Carbon;

/**
 * Funcion para convertir una fecha en con carbon
 * @param string $fecha, Fecha que se desea manipular
 */
function fechaToCarbonHelper(string $fecha)
{
    $f = new DateTime($fecha);
    $fecha = Carbon::create($f->format("Y"), $f->format("m"), $f->format("d"), $f->format("H"), $f->format("i"), $f->format("s"));
    return $fecha;
}
