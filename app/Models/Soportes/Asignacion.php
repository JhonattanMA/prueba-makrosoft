<?php

namespace App\Models\Soportes;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Asignacion extends Model
{
    use HasFactory;

    protected $table = 'asignaciones';

    protected $fillable = ['fecha_asignacion','trabajadores_id','soportes_id'];

    /** RELACIONES */

    public function soporte()
    {
        return $this->belongsTo(Soporte::class, 'soportes_id', 'id');
    }
}
