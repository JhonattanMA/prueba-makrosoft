<?php

namespace App\Models\Trabajadores;

use App\Models\Soportes\Asignacion;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Trabajador extends Model
{
    use HasFactory;

    protected $table = 'trabajadores';

    protected $fillable = ['nombres','apellidos','identificacion','celular','usuarios_id'];

    /** RELACIONES */

    public function usuario()
    {
        return $this->belongsTo(User::class, 'usuarios_id', 'id');
    }

    public function asignaciones()
    {
        return $this->hasMany(Asignacion::class, 'trabajadores_id', 'id');
    }

    public function pesosAcumulados()
    {
        return $this->hasMany(PesoAcumulado::class, 'trabajadores_id', 'id');
    }
}
