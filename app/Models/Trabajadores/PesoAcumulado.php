<?php

namespace App\Models\Trabajadores;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PesoAcumulado extends Model
{
    use HasFactory;

    protected $table = 'pesos_acumulados';

    protected $fillable = ['fecha','peso_acumulado','trabajadores_id'];
}
