<?php

namespace App\Providers;

use App\Services\Soportes\AsignacionService;
use App\Services\Soportes\Implementations\AsignacionServiceImpl;
use App\Services\Soportes\Implementations\SoporteServiceImpl;
use App\Services\Soportes\SoporteService;
use Illuminate\Support\ServiceProvider;

class SoportesServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(SoporteService::class, SoporteServiceImpl::class);
        $this->app->bind(AsignacionService::class, AsignacionServiceImpl::class);
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
