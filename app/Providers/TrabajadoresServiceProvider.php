<?php

namespace App\Providers;

use App\Services\Trabajadores\Implementations\PesoAcumuladoServiceImpl;
use App\Services\Trabajadores\Implementations\TrabajadorServiceImpl;
use App\Services\Trabajadores\PesoAcumuladoService;
use App\Services\Trabajadores\TrabajadorService;
use Illuminate\Support\ServiceProvider;

class TrabajadoresServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(TrabajadorService::class, TrabajadorServiceImpl::class);
        $this->app->bind(PesoAcumuladoService::class, PesoAcumuladoServiceImpl::class);
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
