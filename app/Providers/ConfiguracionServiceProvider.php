<?php

namespace App\Providers;

use App\Services\Configuracion\Implementations\PermisoServiceImpl;
use App\Services\Configuracion\Implementations\RolServiceImpl;
use App\Services\Configuracion\Implementations\UsuarioServiceImpl;
use App\Services\Configuracion\PermisoService;
use App\Services\Configuracion\RolService;
use App\Services\Configuracion\UsuarioService;
use Illuminate\Support\ServiceProvider;

class ConfiguracionServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(UsuarioService::class, UsuarioServiceImpl::class);
        $this->app->bind(RolService::class, RolServiceImpl::class);
        $this->app->bind(PermisoService::class, PermisoServiceImpl::class);
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
