<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\Trabajadores\Trabajador;
use App\Providers\RouteServiceProvider;
use App\Models\User;
use App\Services\Trabajadores\TrabajadorService;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            // 'nombre_usuario' => ['required', 'string', 'max:255'],
            // 'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            // 'password' => ['required', 'string', 'min:8', 'confirmed']
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\Models\User
     */
    protected function create(array $data)
    {
        // return request()->file('avatar');
        // if (request()->has('avatar')) {
        //     $avatar = request()->file('avatar');
        //     $avatarName = time() . '.' . $avatar->getClientOriginalExtension();
        //     $avatarPath = public_path('/images/');
        //     $avatar->move($avatarPath, $avatarName);
        // }

        $usuario =  User::create([
            'nombre_usuario' => $data['nombres'].' '.$data['apellidos'],
            'email' => $data['email'],
            'password' => Hash::make($data['password'])
        ]);

        $this->registrarTrabajador($data, $usuario->id);

        return $usuario;
    }

    public function registrarTrabajador(array $datos, int $usuarioId)
    {
        $datosTrabajador = array(
            'nombres' => $datos['nombres'],
            'apellidos' => $datos['apellidos'],
            'identificacion' => $datos['identificacion'],
            'celular' => $datos['celular'],
            'usuarios_id' => $usuarioId,
        );

        $respuesta = Trabajador::create($datosTrabajador);
    }
}
