<?php

namespace App\Http\Livewire\Soportes\Asignaciones;

use Livewire\Component;

class AsignacionesComponent extends Component
{
    public function render()
    {
        return view('livewire.soportes.asignaciones.asignaciones-component');
    }
}
