<?php

namespace App\Http\Livewire\Soportes\Soportes;

use App\Services\Soportes\AsignacionService;
use App\Services\Soportes\SoporteService;
use Livewire\Component;
use Livewire\WithPagination;

class SoportesComponent extends Component
{
    use WithPagination;

    protected $paginationTheme = 'bootstrap';
    public $soporteId;
    public $nombres;
    public $prioridad;
    public $pesoTrabajo;
    public $descripcion;
    public $datosSoporte;

    protected $rules = [
        'nombres' => ['required', 'string', 'max:50'],
        'prioridad' => ['required', 'numeric'],
        'pesoTrabajo' => ['required', 'numeric'],
        'descripcion' => ['required', 'string']
    ];

    public function getSoporteServiceProperty() : SoporteService
    {
        return resolve(SoporteService::class);
    }

    public function getAsignacionServiceProperty() : AsignacionService
    {
        return resolve(AsignacionService::class);
    }

    public function render()
    {
        $soportes = $this->soporteService->obtenerSoportes()->paginate();
        $this->dispatchBrowserEvent('renderizarSelects');

        return view('livewire.soportes.soportes.soportes-component', compact('soportes'));
    }

    public function registrar()
    {
        $this->validate();

        $datos = $this->obtenerDatos();
        $respuesta = $this->SoporteService->registrarSoporte($datos);

        if (!$respuesta){
            session()->flash('error', 'Error al registrar el soporte!');
        }

        $this->reset();
        session()->flash('success', 'El soporte fue registrado correctamente!');
        $this->dispatchBrowserEvent('cerrarModal',['modal' => 'modalRegistrarSoporte']);
    }

    public function editar(int $soporteId)
    {
        $cliente = $this->SoporteService->obtenerSoporte($soporteId);
        $this->soporteId = $soporteId;
        $this->nombres = $cliente->nombres;
        $this->prioridad = $cliente->prioridad;
        $this->pesoTrabajo = $cliente->peso_trabajo;
        $this->descripcion = $cliente->descripcion;

        $this->dispatchBrowserEvent('abrirModal', ['modal' => 'modalActualizarSoporte']);
    }

    public function actualizar()
    {
        $this->validate();

        $datos = $this->obtenerDatos();
        $respuesta = $this->SoporteService->actualizarSoporte($this->soporteId, $datos);

        if (!$respuesta){
            session()->flash('error', 'Error al actualizar el soporte!');
        }

        $this->reset();
        session()->flash('success', 'El soporte fue actualizado correctamente!');
        $this->dispatchBrowserEvent('cerrarModal',['modal' => 'modalActualizarSoporte']);
    }

    public function verDetalles($soporteId)
    {
        $this->datosSoporte = $this->SoporteService->obtenerSoporte($soporteId);
        $this->dispatchBrowserEvent('abrirModal',['modal' => 'modalDetalleSoporte']);
    }

    public function obtenerDatos()
    {
        return array(
            'nombres' => $this->nombres,
            'prioridad' => $this->prioridad,
            'peso_trabajo' => $this->pesoTrabajo,
            'descripcion' => $this->descripcion
        );
    }

    public function asignarSoportes()
    {
        $respuesta = $this->asignacionService->asignarSoportes();

        if ($respuesta == 0){
            session()->flash('warning', 'No hay soportes para asignar!');
        }elseif (!$respuesta){
            session()->flash('error', 'Error al asignar los soportes!');
        }else{
            session()->flash('success', 'Los soporte fueron asignados correctamente!');
        }
    }

    public function cancelar()
    {
        $this->reset();
    }
}
