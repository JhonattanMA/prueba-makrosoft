<?php

namespace App\Http\Livewire\Trabajadores\PesosAcumuladoss;

use Livewire\Component;

class PesosAcumuladosComponent extends Component
{
    public function render()
    {
        return view('livewire.trabajadores.pesos-acumuladoss.pesos-acumulados-component');
    }
}
