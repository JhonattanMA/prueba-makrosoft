<?php

namespace App\Http\Livewire\Trabajadores\Trabajadores;

use App\Services\Trabajadores\TrabajadorService;
use Livewire\Component;
use Livewire\WithPagination;

class TrabajadoresComponent extends Component
{
    use WithPagination;

    protected $paginationTheme = 'bootstrap';
    public $trabajadorId;
    public $nombres;
    public $apellidos;
    public $identificacion;
    public $celular;
    public $datosTrabajador;

    protected $rules = [
        'nombres' => ['required', 'string', 'max:50'],
        'apellidos' => ['required', 'string', 'max:50'],
        'identificacion' => ['required', 'string', 'max:20'],
        'celular' => ['required', 'string', 'max:20'],
    ];

    public function getTrabajadorServiceProperty() : TrabajadorService
    {
        return resolve(TrabajadorService::class);
    }

    public function render()
    {
        $trabajadores = $this->trabajadorService->obtenerTrabajadores()->paginate();
        $this->dispatchBrowserEvent('renderizarSelects');

        return view('livewire.trabajadores.trabajadores.trabajadores-component', compact('trabajadores'));
    }

    public function editar(int $trabajadorId)
    {
        $cliente = $this->trabajadorService->obtenerTrabajador($trabajadorId);
        $this->trabajadorId = $trabajadorId;
        $this->nombres = $cliente->nombres;
        $this->apellidos = $cliente->apellidos;
        $this->identificacion = $cliente->identificacion;
        $this->celular = $cliente->celular;

        $this->dispatchBrowserEvent('abrirModal', ['modal' => 'modalActualizarTrabajador']);
    }

    public function actualizar()
    {
        $this->validate();

        $datos = $this->obtenerDatos();
        $respuesta = $this->trabajadorService->actualizarTrabajador($this->trabajadorId, $datos);

        if (!$respuesta){
            session()->flash('error', 'Error al actualizar el trabajador!');
        }

        $this->reset();
        session()->flash('success', 'El trabajador fue actualizado correctamente!');
        $this->dispatchBrowserEvent('cerrarModal',['modal' => 'modalActualizarTrabajador']);
    }

    public function verDetalles($trabajadorId)
    {
        $this->datosTrabajador = $this->trabajadorService->obtenerTrabajador($trabajadorId);
        $this->dispatchBrowserEvent('abrirModal',['modal' => 'modalDetalleTrabajador']);
    }

    public function obtenerDatos()
    {
        return array(
            'nombres' => $this->nombres,
            'apellidos' => $this->apellidos,
            'identificacion' => $this->identificacion,
            'celular' => $this->celular
        );
    }

    public function cancelar()
    {
        $this->reset();
    }
}
