<?php

namespace App\Http\Livewire\Configuracion\Usuarios;

use App\Services\Configuracion\PermisoService;
use App\Services\Configuracion\RolService;
use App\Services\Configuracion\UsuarioService;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rule;
use Livewire\Component;
use Livewire\WithPagination;

class UsuariosComponent extends Component
{
    use WithPagination;

    protected $paginationTheme = 'bootstrap';
    public $usuarioId;
    public $nombre_completo;
    public $email;
    public $password = null;
    public $password_confirmation = null;
    public $rol;
    public $permisos;
    public $asignarRemover;
    public $condiciones;
    public $mostrarContrasena = 1;

    protected $rules = [
        'nombre_completo' => ['required', 'string', 'max:255'],
        'email' => ['required', 'string', 'email', 'max:255', 'unique:users,email'],
        'password' => ['required', 'string', 'min:8', 'confirmed']
    ];

    public function getUsuarioServiceProperty() : UsuarioService
    {
        return resolve(UsuarioService::class);
    }

    public function getRolServiceProperty() : RolService
    {
        return resolve(RolService::class);
    }

    public function getPermisoServiceProperty(): PermisoService
    {
        return resolve(PermisoService::class);
    }

    public function render()
    {
        $usuarios = $this->UsuarioService->obtenerUsuarios()->where($this->condiciones)->paginate();
        $roles = $this->RolService->obtenerRoles()->get();
        $datosPermisos = $this->PermisoService->obtenerPermisos()->get();
        $this->dispatchBrowserEvent('renderizarSelects');

        return view('livewire.configuracion.usuarios.usuarios-component', compact('usuarios','roles','datosPermisos'));
    }

    public function registrar()
    {
        $this->validate();

        $datos = $this->obtenerDatos();
        $respuesta = $this->usuarioService->resgistrarUsuario($datos);

        if (!$respuesta){
            session()->flash('error', 'Error al registrar el usuario!');
        }

        $this->reset();
        session()->flash('success', 'El usuario fue registrado correctamente!');
        $this->dispatchBrowserEvent('cerrarModal',['modal' => 'modalRegistrarUsuario']);
    }

    public function editar(int $usuarioId)
    {
        $usuario = $this->usuarioService->obtenerUsuario($usuarioId);
        $this->usuarioId = $usuarioId;
        $this->nombre_completo = $usuario->nombre_completo;
        $this->email = $usuario->email;

        $this->dispatchBrowserEvent('abrirModal', ['modal' => 'modalActualizarUsuario']);
    }

    public function actualizar()
    {
        $this->validate([
            'nombre_completo' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255']
        ]);
        Rule::unique('users')->ignore($this->usuarioId);

        $datos = $this->obtenerDatos();

        if ($this->mostrarContrasena == 1) {
            unset($datos['password']);
        }

        $respuesta = $this->usuarioService->actualizarUsuario($this->usuarioId, $datos);

        if (!$respuesta){
            session()->flash('error', 'Error al actualizar el usuario!');
        }

        $this->reset();
        session()->flash('success', 'El usuario fue actualizado correctamente!');
        $this->dispatchBrowserEvent('cerrarModal', ['modal' => 'modalActualizarUsuario']);
    }

    public function obtenerIdUsuario(int $usuarioId, string $asignarRemover = null)
    {
        $this->usuarioId = $usuarioId;
        $usuario = $this->UsuarioService->obtenerUsuario($usuarioId);
        $permisos = array();

        foreach ($usuario->permissions as $permiso){
            array_push($permisos,$permiso->id);
        }

        $this->permisos = $permisos;

        if ($asignarRemover){
            $this->asignarRemover = $asignarRemover;
            $this->dispatchBrowserEvent('abrirModal',['modal' => 'modalAsignarRemoverRol']);
        }else {
            $this->dispatchBrowserEvent('abrirModal',['modal' => 'modalAsignarPermisos']);
        }
    }

    public function asignarRolAUsuario()
    {
        $this->validate(['rol' => 'required']);
        $respuesta = $this->UsuarioService->asignarRolAUsuario($this->usuarioId, $this->rol);

        if (!$respuesta) {
            session()->flash('error', 'Error al asignar el rol!');
        }

        $this->reset();
        session()->flash('success', 'El rol fue asignado correctamente!');
        $this->dispatchBrowserEvent('cerrarModal',['modal' => 'modalAsignarRemoverRol']);
    }

    public function removerRolAUsuario()
    {
        $this->validate(['rol' => 'required']);
        $respuesta = $this->UsuarioService->removerRolAUsuario($this->usuarioId, $this->rol);

        if (!$respuesta) {
            session()->flash('error', 'Error al remover el rol!');
        }

        $this->reset();
        session()->flash('success', 'El rol fue removido correctamente!');
        $this->dispatchBrowserEvent('cerrarModal',['modal' => 'modalAsignarRemoverRol']);
    }

    public function asignarPermisos()
    {
        $respuesta = $this->UsuarioService->asignarPermisosAUsuario($this->usuarioId, $this->permisos);

        if (!$respuesta) {
            session()->flash('error', 'Error al asignar los permisos!');
        }

        $this->reset();
        session()->flash('success', 'Los permisos fueron asignados correctamente!');
        $this->dispatchBrowserEvent('cerrarModal',['modal' => 'modalAsignarPermisos']);
    }

    function obtenerDatos() {
        return array(
            'nombre_completo' => ucwords($this->nombre_completo),
            'email' => $this->email,
            'password' => Hash::make($this->password)
        );
    }

    public function cambiarContrasena()
    {
        $this->mostrarContrasena++;
    }

    public function ocultarContrasena()
    {
        $this->mostrarContrasena--;
    }

    public function cancelar()
    {
        $this->reset();
    }
}
