<?php

namespace App\Http\Livewire\Configuracion\Roles;

use App\Services\Configuracion\PermisoService;
use App\Services\Configuracion\RolService;
use Livewire\Component;
use Livewire\WithPagination;

class RolesComponent extends Component
{
    use WithPagination;

    protected $paginationTheme = 'bootstrap';
    public $nombre;
    public $rolId;
    public $permisos;
    public $permisosAsignados;
    public $condiciones;

    protected $rules = [
        'nombre' => 'required'
    ];

    public function getRolServiceProperty(): RolService
    {
        return resolve(RolService::class);
    }

    public function getPermisoServiceProperty(): PermisoService
    {
        return resolve(PermisoService::class);
    }

    public function render()
    {
        $roles = $this->rolService->obtenerRoles()->where($this->condiciones)->paginate();
        $datosPermisos = $this->permisoService->obtenerPermisos()->get();
        $this->dispatchBrowserEvent('renderizarSelects');

        return view('livewire.configuracion.roles.roles-component', compact('roles', 'datosPermisos'));
    }

    public function registrar()
    {
        $this->validate();

        $nombre = ucfirst(trim(strtolower($this->nombre)));
        $rol = $this->rolService->obtenerRolPorNombre($nombre);

        if ($rol && $rol->name == $nombre){
            return redirect(request()->header('Referer'))->with('info', 'El rol ' . $nombre . ' ya se encuentra en uso.');
        }

        $respuesta = $this->rolService->registrarRol($nombre);

        if (!$respuesta){
            session()->flash('error', 'Error al registrar el rol!');
        }

        $this->reset();
        session()->flash('success', 'El rol fue registrado correctamente!');
        $this->dispatchBrowserEvent('cerrarModal',['modal' => 'modalRegistrarRol']);
    }

    public function editar(int $rolId)
    {
        $rol = $this->rolService->obtenerRol($rolId);
        $this->rolId = $rol->id;
        $this->nombre = $rol->name;

        $this->dispatchBrowserEvent('abrirModal', ['modal' => 'modalActualizarRol']);
    }

    public function actualizar()
    {
        $this->validate();
        $nombre = ucfirst(trim(strtolower($this->nombre)));
        $rolPorNombre = $this->rolService->obtenerRolPorNombre($nombre);

        if ($rolPorNombre && $rolPorNombre->name == $nombre && $rolPorNombre->id != $this->rolId){
            return redirect(request()->header('Referer'))->with('info', 'El rol ' . $nombre . ' ya se encuentra en uso.');
        }

        $respuesta = $this->rolService->actualizarRol($this->rolId, $nombre);

        if (!$respuesta){
            session()->flash('error', 'Error al actualizar el rol!');
        }

        $this->reset();
        session()->flash('success', 'El rol fue actualizado correctamente!');
        $this->dispatchBrowserEvent('cerrarModal', ['modal' => 'modalActualizarRol']);
    }

    public function obtenerIdRol(int $rolId)
    {
        $this->rolId = $rolId;
        $rol = $this->rolService->obtenerRol($rolId);
        $permisos = array();

        foreach ($rol->permissions as $permiso){
            array_push($permisos,$permiso->id);
        }

        $this->permisos = $permisos;
        $this->dispatchBrowserEvent('abrirModal', ['modal' => 'modalAsignarPermisos']);
    }

    public function asignarPermisos(){
        $respuesta = $this->rolService->asignarPermisosARol($this->rolId, $this->permisos);

        if (!$respuesta){
            session()->flash('error', 'Error al asignar los permisos!');
        }

        $this->reset();
        session()->flash('success', 'Los permisos fueron asignados correctamente!');
        $this->dispatchBrowserEvent('cerrarModal', ['modal' => 'modalAsignarPermisos']);
    }

    public function cancelar()
    {
        $this->reset();
        // return redirect(request()->header('Referer'));
    }
}
