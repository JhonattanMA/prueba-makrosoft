<?php

namespace App\Http\Livewire\Configuracion\Permisos;

use App\Services\Configuracion\PermisoService;
use Livewire\Component;
use Livewire\WithPagination;

class PermisosComponent extends Component
{
    use WithPagination;

    protected $paginationTheme = 'bootstrap';
    public $nombre;
    public $permisoId;
    public $condiciones;

    protected $rules = [
        'nombre' => 'required'
    ];

    public function getPermisoServiceProperty() : PermisoService
    {
        return resolve(PermisoService::class);
    }

    public function render()
    {
        $permisos = $this->permisoService->obtenerPermisos()->where($this->condiciones)->paginate();
        return view('livewire.configuracion.permisos.permisos-component', compact('permisos'));
    }

    public function registrar()
    {
        $this->validate();

        $nombre = trim(strtolower($this->nombre));
        $permiso = $this->permisoService->obtenerPermisoPorNombre($nombre);

        if ($permiso && $permiso->name == $nombre){
            return redirect(request()->header('Referer'))->with('info', 'El permiso '. $nombre .' ya se encuentra en uso.');
        }

        $respuesta = $this->permisoService->registrarPermiso($nombre);

        $this->reset();
        $this->dispatchBrowserEvent('cerrarModal',['modal' => 'modalRegistrarPermiso']);

        if (!$respuesta) {
            return session()->flash('error', 'Error al registrar el permiso!');
        }

        return session()->flash('success', 'El permiso fue registrado correctamente!');
    }

    public function editar(int $permisoId)
    {
        $permiso = $this->permisoService->obtenerPermiso($permisoId);
        $this->permisoId = $permiso->id;
        $this->nombre = $permiso->name;
        $this->dispatchBrowserEvent('abrirModal',['modal' => 'modalActualizarPermiso']);
    }

    public function actualizar()
    {
        $this->validate();
        $nombre = trim(strtolower($this->nombre));
        $permisoPorNombre = $this->permisoService->obtenerPermisoPorNombre($nombre);

        if ($permisoPorNombre && $permisoPorNombre->name == $nombre && $permisoPorNombre->id != $this->permisoId) {
            return redirect(request()->header('Referer'))->with('info', 'El permiso '. $nombre .' ya se encuentra en uso.');
        }

        $respuesta = $this->permisoService->actualizarPermiso($this->permisoId,$nombre);

        $this->reset();
        $this->dispatchBrowserEvent('cerrarModal',['modal' => 'modalActualizarPermiso']);

        if (!$respuesta) {
            return session()->flash('error', 'Error al actualizar el permiso!');
        }

        return session()->flash('success', 'El permiso fue actualizado correctamente!');
    }

    public function cancelar()
    {
        $this->reset();
    }
}
