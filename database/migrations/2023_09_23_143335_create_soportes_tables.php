<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('soportes', function (Blueprint $table) {
            $table->id();
            $table->string('nombres',50);
            $table->text('descripcion');
            $table->integer('prioridad');
            $table->integer('peso_trabajo');

            //Times
            $table->timestamps();
        });

        Schema::create('asignaciones', function (Blueprint $table) {
            $table->id();
            $table->date('fecha_asignacion');
            $table->unsignedBigInteger('trabajadores_id');
            $table->unsignedBigInteger('soportes_id');

            //FK
            $table->foreign('trabajadores_id')
                ->references('id')
                ->on('trabajadores');

            $table->foreign('soportes_id')
                ->references('id')
                ->on('soportes');

            //Times
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('asignaciones');
        Schema::dropIfExists('soportes');
    }
};
