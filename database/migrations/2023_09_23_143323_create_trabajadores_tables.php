<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trabajadores', function (Blueprint $table) {
            $table->id();
            $table->string('nombres',50);
            $table->string('apellidos',50);
            $table->string('identificacion',20);
            $table->string('celular',20);
            $table->unsignedBigInteger('usuarios_id');

            //FK
            $table->foreign('usuarios_id')
                ->references('id')
                ->on('users');

            //Times
            $table->timestamps();
        });

        Schema::create('pesos_acumulados', function (Blueprint $table) {
            $table->id();
            $table->date('fecha');
            $table->integer('peso_acumulado');
            $table->unsignedBigInteger('trabajadores_id');

            //FK
            $table->foreign('trabajadores_id')
                ->references('id')
                ->on('trabajadores');

            //Times
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pesos_acumulados');
        Schema::dropIfExists('trabajadores');
    }
};
