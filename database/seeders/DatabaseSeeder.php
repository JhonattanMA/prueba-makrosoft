<?php

namespace Database\Seeders;

use Database\Seeders\Clientes\TipoIdentificacionSeeder;
use Database\Seeders\Configuracion\ModelHasRolesSeeder;
use Database\Seeders\Configuracion\PermisosSeeder;
use Database\Seeders\Configuracion\RolesSeeder;
use Database\Seeders\Configuracion\UsuariosSeeder;
use Database\Seeders\Facturas\EstadoFacturaSeeder;
use Database\Seeders\Facturas\TipoPagoSeeder;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        //
    }
}
