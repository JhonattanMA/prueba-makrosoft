<?php

use Illuminate\Support\Facades\Route;

Route::middleware(['can:soportes_ver'])->name('soportes.')->group(function (){
    Route::get('/soportes', function (){
        return view('modulos.soportes.soportes');
    })->name('index');
});
