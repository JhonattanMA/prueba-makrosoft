<?php

use Illuminate\Support\Facades\Route;

Route::middleware(['can:permisos_ver'])->name('permisos.')->group(function (){
    Route::get('/permisos', function (){
        return view('modulos.configuracion.permisos');
    })->name('index');
});
