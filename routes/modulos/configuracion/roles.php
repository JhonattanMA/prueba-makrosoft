<?php

use Illuminate\Support\Facades\Route;

Route::middleware(['can:roles_ver'])->name('roles.')->group(function (){
    Route::get('/roles', function (){
        return view('modulos.configuracion.roles');
    })->name('index');
});
