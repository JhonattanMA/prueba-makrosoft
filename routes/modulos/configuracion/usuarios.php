<?php

use Illuminate\Support\Facades\Route;

Route::middleware(['can:usuarios_ver'])->name('usuarios.')->group(function (){
    Route::get('/usuarios', function (){
        return view('modulos.configuracion.usuarios');
    })->name('index');
});
