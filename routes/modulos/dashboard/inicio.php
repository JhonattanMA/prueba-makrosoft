<?php

use Illuminate\Support\Facades\Route;

Route::name('inicio.')->group(function (){
    Route::get('/inicio', function (){
        return view('modulos.dashboards.inicio');
    })->name('index');
});
