<?php

use Illuminate\Support\Facades\Route;

Route::middleware(['can:trabajadores_ver'])->name('trabajadores.')->group(function (){
    Route::get('/trabajadores', function (){
        return view('modulos.trabajadores.trabajadores');
    })->name('index');
});
