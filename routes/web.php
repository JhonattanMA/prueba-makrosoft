<?php

use App\Http\Controllers\HomeController;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();


//Language Translation
Route::get('index/{locale}', [HomeController::class, 'lang']);

Route::get('/', function (){
    return redirect('/login');
});

// Ruta de prueba
Route::get('/prueba', function (){

});

/**
 * Dashboard
 */

Route::middleware(['auth'])->group(function (){
    require base_path('routes/modulos/dashboard/inicio.php');
});

/**
 * Trabajadores
 */
Route::middleware(['auth','can:trabajadores_ver'])->group(function (){
    require base_path('routes/modulos/trabajadores/trabajadores.php');
});

/**
 * Soportes
 */
Route::middleware(['auth','can:soportes_ver'])->group(function (){
    require base_path('routes/modulos/soportes/soportes.php');
});


//Update User Details
// Route::post('/update-profile/{id}', [HomeController::class, 'updateProfile'])->name('updateProfile');
// Route::post('/update-password/{id}', [HomeController::class, 'updatePassword'])->name('updatePassword');

/**
 * Funcion para linquear la carpeta store a la carpeta public
 * y asi poder ver las imagenes subidas
 */
Route::get('/storage-link', function ()
{
    Artisan::call('storage:link');
});

/**
 * Funcion para limpiar el cache del proyecto
 */
Route::get('/clear-cache', function ()
{
    $exitCode = Artisan::call('cache:clear');
    return '<h1>Cache facade value cleared</h1>';
});

Route::get('{any}', [HomeController::class, 'index'])->name('index');
