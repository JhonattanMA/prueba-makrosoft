/**
 * Con estas funciones se visualizan los tooltips de bootstrap
 * @author Jhonatan Mariaca
 * @fecha 14-06-2024
 */
var tooltipTriggerList = [].slice.call(document.querySelectorAll('[data-bs-toggle="tooltip"]'))
var tooltipList = tooltipTriggerList.map(function (tooltipTriggerEl) {
    return new bootstrap.Tooltip(tooltipTriggerEl)
})

/**
 * Funcion que permite ingresar solo numeros en los inputs
 * @author Jhonatan Mariaca
 * @fecha 14-06-2024
 */

 function soloNumeros(e, input) {
    var key = e.charCode;
    var size = input.value.length;
    if (size > 0) {
        if (!(key >= 48 && key <= 57)) {
            return e.preventDefault();
        }
    } else {
        if (!((key >= 48 && key <= 57) || key == 45)) {
            return e.preventDefault();
        }
    }
}

/**
 * Funcion que permite ingresar solo numeros con decimales
 * @author Jhonatan Mariaca
 * @fecha 14-06-2024
 */
function soloDecimales(e, input) {
    var key = e.charCode;
    var size = input.value.length;

    if (size > 0) {
        if (!((key >= 48 && key <= 57))) {

            if (key == 44) {
                var menos = input.value.indexOf('-');
                var coma = input.value.indexOf(',');
                if(menos > -1 && size < 2){
                    return e.preventDefault();
                }
                else if (coma > -1) {
                    return e.preventDefault();
                }
            }else{
                return e.preventDefault();
            }
        }
    } else {
        if (!((key >= 48 && key <= 57) || key == 45)) {
            return e.preventDefault();
        }
    }
}

/**
 * Funcion para poner los puntos de mil en los valores ingresados
 * @author Jhonatan Mariaca
 * @fecha 14-06-2024
 */
function formatoMiles(input) {
    var size = input.value.length;
    if (size > 0) {
        var num = input.value.replace(/\./g, '');
        if (!isNaN(num)) {
            var text = Number(num).toLocaleString('es-CO');
            input.value = text;
        }

    }
}


/** ----------------------------------- FUNCIONES DE LIVEWIRE ----------------------------------------------- */

/**
 * Funcion para cerrar un modal desde livewire
 * @author Jhonatan Mariaca
 * @date 11-02-2023
 */
 window.addEventListener('cerrarModal', event => {
    $('#'+event.detail.modal).modal('hide');
})

/**
 * Funcion para abrir un modal desde livewire
 * @author Jhonatan Mariaca
 * @date 11-02-2023
 */
window.addEventListener('abrirModal', event => {
    $('#'+event.detail.modal).modal('show');
})

/** ---------------------------------- FIN FUNCIONES DE LIVEWIRE ---------------------------------------------- */
