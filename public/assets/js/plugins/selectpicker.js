/**
 * Funcion para establecer el valor seleccionado en select
 * usando el complemento selectpicker
 * @author Jhonatan Mariaca
 * @date 10-11-2023
 */
 window.livewire.on('seleccionarOpcion', (name,idOpcion) => {
    $('select[name='+name+']').selectpicker('val', idOpcion);
});

/**
 * Funcio para renderizar los selects
 * usando el complemento selectpicker
 * @author Jhonatan Mariaca
 * @fecha 10-11-2023
 */
window.addEventListener('renderizarSelects', event => {
    $('.selectpicker').selectpicker();
});

/**
 * Funcion para refrescar la eleccion del select
 * usando el complemento selectpicker
 * @author Jhonatan Mariaca
 * @fecha 10-11-2023
 */
window.addEventListener('refrescarSelects', event => {
    $('.selectpicker').selectpicker('refresh');
});


/**
 * Funcion para eliminar el selectpicker
 * @author Jhonatan Mariaca
 * @fecha 10-11-2023
 */
document.addEventListener("DOMContentLoaded", () => {
    Livewire.hook('message.received', (message, component) => {
        $('.selectIgnore').selectpicker('destroy');
    })
});
