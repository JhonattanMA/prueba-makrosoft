
 window.addEventListener('renderizarFechas', event => {
    /** ----------------------------------- FUNCIONES DE MATERIAL-DATETIMEPICKER ----------------------------------------------- */
    $(document).ready(function() {

        $('.date-time-picker').bootstrapMaterialDatePicker
        ({
            time: true,
            date: true,
            shortTime: false,
            lang : 'es',
            format: 'YYYY-MM-DD HH:mm'
        });

        $('.date-picker').bootstrapMaterialDatePicker
        ({
            date: true,
            time: false,
            shortTime: false,
            format: 'YYYY-MM-DD',
        });

        $('.time-picker').bootstrapMaterialDatePicker
        ({
            date: false,
            time: true,
            shortTime: false,
            format: 'HH:mm'
        });

        $('.date-end').bootstrapMaterialDatePicker
        ({
            time: true,
            date: true,
            shortTime: false,
            format: 'YYYY-MM-DD HH:mm'
        });

        $('.date-start').bootstrapMaterialDatePicker
        ({
            time: true,
            date: true,
            shortTime: false,
            format: 'YYYY-MM-DD HH:mm'
        }).on('change', function(e, date)
        {
            $('.date-end').bootstrapMaterialDatePicker('setMinDate', date);
        });
    });
});

/** ----------------------------------- FUNCIONES DE MATERIAL-DATETIMEPICKER ----------------------------------------------- */

$(document).ready(function() {

    $('.date-time-picker').bootstrapMaterialDatePicker
    ({
        time: true,
        date: true,
        shortTime: false,
        lang : 'es',
        format: 'YYYY-MM-DD HH:mm'
    });

    $('.date-picker').bootstrapMaterialDatePicker
    ({
        date: true,
        time: false,
        shortTime: false,
        format: 'YYYY-MM-DD',
    });

    $('.time-picker').bootstrapMaterialDatePicker
    ({
        date: false,
        time: true,
        shortTime: false,
        format: 'HH:mm'
    });

    $('.date-end').bootstrapMaterialDatePicker
    ({
        time: true,
        date: true,
        shortTime: false,
        format: 'YYYY-MM-DD HH:mm'
    });

    $('.date-start').bootstrapMaterialDatePicker
    ({
        time: true,
        date: true,
        shortTime: false,
        format: 'YYYY-MM-DD HH:mm'
    }).on('change', function(e, date)
    {
        $('.date-end').bootstrapMaterialDatePicker('setMinDate', date);
    });
});


