@extends('layouts.master')
@section('title') Trabajadores  @endsection

@section('content')
    @component('components.breadcrumb')
        @slot('li_1') Trabajadores @endslot
        @slot('title') Trabajadores  @endslot
    @endcomponent

    @livewire('trabajadores.trabajadores.trabajadores-component')
@endsection
