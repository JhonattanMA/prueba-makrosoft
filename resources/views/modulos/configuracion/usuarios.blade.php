@extends('layouts.master')
@section('title') @lang('translation.users')  @endsection

@section('content')
    @component('components.breadcrumb')
        @slot('li_1') @lang('translation.settings') @endslot
        @slot('title') @lang('translation.users')  @endslot
    @endcomponent

    @livewire('configuracion.usuarios.usuarios-component')
@endsection
