@extends('layouts.master')
@section('title') Roles  @endsection

@section('content')
    @component('components.breadcrumb')
        @slot('li_1') @lang('translation.settings') @endslot
        @slot('title') Roles  @endslot
    @endcomponent

    @livewire('configuracion.roles.roles-component')
@endsection
