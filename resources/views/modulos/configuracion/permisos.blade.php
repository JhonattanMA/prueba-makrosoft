@extends('layouts.master')
@section('title') @lang('translation.permissions')  @endsection

@section('content')
    @component('components.breadcrumb')
        @slot('li_1') @lang('translation.settings') @endslot
        @slot('title') @lang('translation.permissions')  @endslot
    @endcomponent

    @livewire('configuracion.permisos.permisos-component')
@endsection
