@extends('layouts.master')
@section('title') @lang('translation.home')  @endsection

@section('content')
    @component('components.breadcrumb')
        @slot('li_1') @lang('translation.dashboards') @endslot
        @slot('title') @lang('translation.home')  @endslot
    @endcomponent
@endsection
