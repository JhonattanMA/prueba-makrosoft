@extends('layouts.master')
@section('title') Soportes  @endsection

@section('content')
    @component('components.breadcrumb')
        @slot('li_1') Soportes @endslot
        @slot('title') Soportes  @endslot
    @endcomponent

    @livewire('soportes.soportes.soportes-component')
@endsection
