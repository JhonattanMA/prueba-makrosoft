@yield('css')
<!-- Layout config Js -->
<script src="{{ URL::asset('assets/js/layout.js') }}"></script>
<!-- Bootstrap Css -->
<link href="{{ URL::asset('assets/css/bootstrap.min.css') }}"  rel="stylesheet" type="text/css" />
<!-- Icons Css -->
<link href="{{ URL::asset('assets/css/icons.min.css') }}" rel="stylesheet" type="text/css" />
<!-- App Css-->
<link href="{{ URL::asset('assets/css/app.min.css') }}"  rel="stylesheet" type="text/css" />
<!-- custom Css-->
<link href="{{ URL::asset('assets/css/custom.min.css') }}"  rel="stylesheet" type="text/css" />
<!-- Plugins -->
<link href="{{ URL::asset('assets/css/plugins/bootstrap-select.min.css') }}"  rel="stylesheet" type="text/css" />
<link href="{{ URL::asset('assets/css/plugins/bootstrap-material-datetimepicker.css') }}"  rel="stylesheet" type="text/css" />
<link href="{{ URL::asset('assets/css/plugins/datetimepicker.css') }}"  rel="stylesheet" type="text/css" />
<!-- Estilos Plantilla Css-->
<link href="{{ URL::asset('assets/css/estilos-plantilla.css') }}"  rel="stylesheet" type="text/css" />
{{-- @yield('css') --}}
