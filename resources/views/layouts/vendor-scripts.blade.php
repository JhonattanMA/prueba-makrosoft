<script src="{{ URL::asset('assets/libs/bootstrap/bootstrap.min.js') }}"></script>
<script src="{{ URL::asset('assets/libs/simplebar/simplebar.min.js') }}"></script>
<script src="{{ URL::asset('assets/libs/node-waves/node-waves.min.js') }}"></script>
<script src="{{ URL::asset('assets/libs/feather-icons/feather-icons.min.js') }}"></script>
<script src="{{ URL::asset('assets/js/pages/plugins/lord-icon-2.1.0.min.js') }}"></script>
<script src="{{ URL::asset('assets/js/plugins.min.js') }}"></script>
<script src="{{ URL::asset('assets/js/app.min.js') }}"></script>
<script src="{{ URL::asset('assets/js/jquery.min.js') }}"></script>
<script src="{{ URL::asset('assets/js/moment.min.js') }}"></script>

<!-- Plugins -->
<script src="{{ URL::asset('assets/js/plugins/bootstrap-select.min.js') }}"></script>
<script src="{{ URL::asset('assets/js/plugins/bootstrap-material-datetimepicker.js') }}"></script>
<!-- Funciones creadas -->
<script src="{{ URL::asset('assets/js/scripts-principales.js') }}"></script>
<script src="{{ URL::asset('assets/js/plugins/selectpicker.js') }}"></script>
<script src="{{ URL::asset('assets/js/plugins/datepicker.js') }}"></script>

@yield('script')
@yield('script-bottom')
