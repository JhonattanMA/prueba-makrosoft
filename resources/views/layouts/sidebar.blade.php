<!-- ========== App Menu ========== -->
<div class="app-menu navbar-menu">
    <!-- LOGO -->
    <div class="navbar-brand-box">
        <!-- Dark Logo-->
        <a href="{{ route('inicio.index') }}" class="logo logo-dark">
            <span class="logo-lg">
                <img src="{{ URL::asset('assets/images/logo-dark.png') }}" alt="" width="110">
            </span>
        </a>
        <!-- Light Logo-->
        <a href="{{ route('inicio.index') }}" class="logo logo-light">
            <span class="logo-lg">
                <img src="{{ URL::asset('assets/images/logo-light.png') }}" alt="" height="50">
            </span>
        </a>
        <button type="button" class="btn btn-sm p-0 fs-20 header-item float-end btn-vertical-sm-hover" id="vertical-hover">
            <i class="ri-record-circle-line"></i>
        </button>
    </div>

    <div id="scrollbar">
        <div class="container-fluid">

            <div id="two-column-menu">
            </div>
            <ul class="navbar-nav" id="navbar-nav">
                <li class="menu-title"><span>@lang('translation.menu')</span></li>
                <li class="nav-item">
                    <a class="nav-link menu-link" href="#sidebarDashboards" data-bs-toggle="collapse" role="button" aria-expanded="false" aria-controls="sidebarDashboards">
                        <i data-feather="home" class="icon-dual"></i> <span>@lang('translation.dashboards')</span>
                    </a>
                    <div class="collapse menu-dropdown" id="sidebarDashboards">
                        <ul class="nav nav-sm flex-column">
                            <li class="nav-item">
                                <a href="{{ route('inicio.index') }}" data-route="inicio" class="nav-link">@lang('translation.home')</a>
                            </li>
                        </ul>
                    </div>
                </li> <!-- end Dashboard Menu -->

                <li class="nav-item">
                    <a class="nav-link menu-link" href="#sidebarTrabajadores" data-bs-toggle="collapse" role="button" aria-expanded="false" aria-controls="sidebarCustomers">
                        <i data-feather="users" class="icon-dual"></i> <span>Trabajadores</span>
                    </a>
                    <div class="collapse menu-dropdown" id="sidebarTrabajadores">
                        <ul class="nav nav-sm flex-column">
                            <li class="nav-item">
                                <a href="{{ route('trabajadores.index') }}" data-route="trabajadores" class="nav-link">Trabajadores</a>
                            </li>
                        </ul>
                    </div>
                </li><!-- end Clientes -->

                <li class="nav-item">
                    <a class="nav-link menu-link" href="#sidebarSoportes" data-bs-toggle="collapse" role="button" aria-expanded="false" aria-controls="sidebarCustomers">
                        <i data-feather="file-minus" class="icon-dual"></i> <span>Soportes</span>
                    </a>
                    <div class="collapse menu-dropdown" id="sidebarSoportes">
                        <ul class="nav nav-sm flex-column">
                            <li class="nav-item">
                                <a href="{{ route('soportes.index') }}" data-route="soportes" class="nav-link">Soportes</a>
                            </li>
                        </ul>
                    </div>
                </li><!-- end Clientes -->
            </ul>
        </div>
        <!-- Sidebar -->
    </div>
    <div class="sidebar-background"></div>
</div>
<!-- Left Sidebar End -->
<!-- Vertical Overlay-->
<div class="vertical-overlay"></div>
