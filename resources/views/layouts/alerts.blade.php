@if (session('success'))
    <x-alert tipo="success"></x-alert>

    @php
    	$flag = true;
    @endphp
@endif

@if (session('info'))
    <x-alert tipo="info"></x-alert>

    @php
    	$flag = true;
    @endphp
@endif

@if (session('warning'))
    <x-alert tipo="warning"></x-alert>

    @php
        $flag = true;
    @endphp
@endif

@if (session('error'))
    <x-alert tipo="error"></x-alert>

    @php
    	$flag = true;
    @endphp
@endif

{{-- @if ($errors->any())
    <div class="col-11 mx-auto" id="notificacion">
        <div class="alert alert-danger alert-border-left alert-dismissible fade show" role="alert">
            <i class="ri-close-circle-line me-3 align-middle"></i>
            {{ session(isset($tipo) ? $tipo : '') }}
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
        </div>
    </div>
    @php
    	$flag = true;
    @endphp
@endif --}}

@if (isset($flag) && $flag)
    <script>
        function myGreeting() {
            document.getElementById("notificacion").style.display = 'none';
        }

        const myTimeout = setTimeout(myGreeting, 10000);
    </script>
@endif
