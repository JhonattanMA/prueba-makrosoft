<div>
    <!-- Alertas -->
    @include('layouts.alerts')

    @include('livewire.soportes.soportes.crear')
    @include('livewire.soportes.soportes.editar')
    @include('livewire.soportes.soportes.detalle')

    <div class="row g-4 mb-3">
        <div class="col-sm-auto">
            <div>
                @can('soporte_registrar')
                    @component('components.button', [
                        'color' => 'success',
                        'icono' => 'ri-add-circle-line',
                        'titulo' => 'Registrar'
                    ])
                        data-bs-toggle="modal" data-bs-target="#modalRegistrarSoporte"
                    @endcomponent
                @endcan

                @can('soporte_asignar')
                    <a class="btn btn-primary btn-label waves-effect waves-light" wire:click="asignarSoportes()">
                        <i class="ri-mail-send-line label-icon align-middle fs-20 me-2"></i> Asignar
                    </a>
                @endcan
            </div>
        </div>
    </div>

    <div class="mt-3">
        @component('components.card')
            @component('components.table')
                @slot('thead')
                    <th scope="col">N°</th>
                    <th scope="col">NOMBRE</th>
                    <th scope="col">DESCRIPCION</th>
                    <th scope="col">PRIORIDAD</th>
                    <th scope="col">PESO TRABAJO</th>
                    <th scope="col">ACCIONES</th>
                @endslot
                @slot('tbody')
                    @foreach ($soportes as $soporte)
                        <tr>
                            <td scope="row" data-titulo="N°">{{ $loop->iteration }}</td>
                            <td scope="row" data-titulo="NOMBRE">{{ $soporte->nombres }}</td>
                            <td scope="row" data-titulo="DESCRIPCION">{{ $soporte->descripcion }}</td>
                            <td scope="row" data-titulo="PRIORIDAD">
                                @if ($soporte->prioridad == 1 || $soporte->prioridad == 2)
                                    <span class="badge badge-label bg-info"><i class="mdi mdi-circle-medium"></i> {{ $soporte->prioridad }}</span>
                                @elseif ($soporte->prioridad == 3)
                                    <span class="badge badge-label bg-warning"><i class="mdi mdi-circle-medium"></i> {{ $soporte->prioridad }}</span>
                                @elseif ($soporte->prioridad == 4 || $soporte->prioridad == 5)
                                    <span class="badge badge-label bg-danger"><i class="mdi mdi-circle-medium"></i> {{ $soporte->prioridad }}</span>
                                @endif
                            </td>
                            <td scope="row" data-titulo="PESO TRABAJO">{{ $soporte->peso_trabajo }}</td>
                            <td scope="row" data-titulo="ACCIONES">
                                @can('soporte_actualizar')
                                    <a class="btn btn-soft-secondary btn-border btn-sm m-1" data-bs-toggle="tooltip" data-bs-placement="top" title="Editar"
                                    wire:click="editar({{ $soporte->id }})">
                                        <i class="ri-edit-box-line"></i>
                                    </a>
                                @endcan
                                <a class="btn btn-soft-secondary btn-border btn-sm m-1" data-bs-toggle="tooltip" data-bs-placement="top" title="Ver"
                                wire:click="verDetalles({{ $soporte->id }})">
                                    <i class="ri-eye-line"></i>
                                </a>
                            </td>
                        </tr>
                    @endforeach
                @endslot
                <tr>
                    <td>Total registros <b>{{ $soportes->total() }}</b></td>
                    <td class="float-end">
                        @if(isset($datosFiltros) && count($datosFiltros) > 0)
                            {{ $soportes->appends($datosFiltros)->links() }}
                        @else
                            {{ $soportes->links() }}
                        @endif
                    </td>
                </tr>
            @endcomponent
        @endcomponent
    </div>
</div>
