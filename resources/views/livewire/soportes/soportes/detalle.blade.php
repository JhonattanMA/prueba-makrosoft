@component('components.modal', [
        'title' => 'Detalle del Soporte',
        'id' => 'modalDetalleSoporte',
        'self' => true,
        'btnCerrar' => true
    ])
    @slot('body')
        @if ($datosSoporte)
            <div class="mt-2 mb-2">
                <h3 class="text-center texto-subrayado">{{ $datosSoporte->nombres }}</h3>
            </div>

            <div>
                <h5 class="mb-3">Información:</h5>
                <div class="ms-2">
                    <div class="d-flex">
                        <strong class="text-dark col-6">Prioridad:</strong>
                        <p class="col-6 text-muted">
                            @if ($datosSoporte->prioridad == 1 || $datosSoporte->prioridad == 2)
                                <span class="badge badge-label bg-info"><i class="mdi mdi-circle-medium"></i> {{ $datosSoporte->prioridad }}</span>
                            @elseif ($datosSoporte->prioridad == 3)
                                <span class="badge badge-label bg-warning"><i class="mdi mdi-circle-medium"></i> {{ $datosSoporte->prioridad }}</span>
                            @elseif ($datosSoporte->prioridad == 4 || $datosSoporte->prioridad == 5)
                                <span class="badge badge-label bg-danger"><i class="mdi mdi-circle-medium"></i> {{ $datosSoporte->prioridad }}</span>
                            @endif
                        </p>
                    </div>

                    <div class="d-flex">
                        <strong class="text-dark col-6">Peso Trabajo:</strong>
                        <p class="col-6 text-muted">{{ $datosSoporte->peso_trabajo }}</p>
                    </div>

                    <div class="d-flex">
                        <strong class="text-dark col-6">Descripción:</strong>
                        <p class="col-6 text-muted">{{ $datosSoporte->descripcion }}</p>
                    </div>
                </div>
            </div>
        @endif
    @endslot
@endcomponent
