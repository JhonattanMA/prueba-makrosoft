@can('soportes_actualizar')
    @component('components.modal', [
            'title' => 'Actualizar Soporte',
            'id' => 'modalActualizarSoporte',
            'static' => true,
            'self' => true,
            'size' => 'modal-lg'
        ])
        @slot('body')
            <div class="row">
                <div class="form-group">
                    <label for="nombres" class="form-label">Nombre <span class="text-danger">*</span></label>
                    <input id="nombres" wire:model="nombres" type="text" class="form-control rounded-pill {{ $errors->has('nombres') ? 'is-invalid' : '' }}">

                    @error('nombres')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>

                <div class="form-group col-lg-6 col-md-6">
                    <label for="prioridad" class="form-label">Prioridad <span class="text-danger">*</span></label>
                    <select wire:model="prioridad" id="prioridad" class="form-control {{ $errors->has('prioridad') ? 'is-invalid' : '' }} selectpicker selectIgnore" data-size="7" data-live-search="true" title="Seleccione...">
                        <option value="">NINGUNO</option>
                        <option value="1">1</option>
                        <option value="2">2</option>
                        <option value="3">3</option>
                        <option value="4">4</option>
                        <option value="5">5</option>
                    </select>

                    @error('prioridad')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>

                <div class="form-group col-lg-6 col-md-6">
                    <label for="pesoTrabajo" class="form-label">Peso Trabajo <span class="text-danger">*</span></label>
                    <select wire:model="pesoTrabajo" id="pesoTrabajo" class="form-control {{ $errors->has('pesoTrabajo') ? 'is-invalid' : '' }} selectpicker selectIgnore" data-size="7" data-live-search="true" title="Seleccione...">
                        <option value="">NINGUNO</option>
                        <option value="1">1</option>
                        <option value="2">2</option>
                        <option value="3">3</option>
                        <option value="4">4</option>
                        <option value="5">5</option>
                    </select>

                    @error('pesoTrabajo')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>

                <div class="form-group">
                    <label for="descripcion" class="form-label">Descripcion <span class="text-danger">*</span></label>
                    <textarea id="descripcion" wire:model="descripcion" type="text" class="form-control rounded-pill {{ $errors->has('descripcion') ? 'is-invalid' : '' }}"></textarea>

                    @error('descripcion')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
            </div>
        @endslot
        @slot('footer')
            @component('components.button', [
                'color' => 'secondary',
                'icono' => 'ri-close-circle-line',
                'titulo' => 'Cancelar'
            ])
                data-bs-dismiss="modal"
                wire:click.prevent="cancelar()"
            @endcomponent

            @component('components.button', [
                'color' => 'success',
                'icono' => 'ri-save-line',
                'titulo' => 'Guardar'
            ])
                wire:loading.attr="disabled" wire:click.prevent="actualizar()"
            @endcomponent
        @endslot
    @endcomponent
@endcan
