<div>
    <!-- Alertas -->
    @include('layouts.alerts')

    @include('livewire.trabajadores.trabajadores.editar')
    @include('livewire.trabajadores.trabajadores.detalle')

    <div class="mt-3">
        @component('components.card')
            @component('components.table')
                @slot('thead')
                    <th scope="col">N°</th>
                    <th scope="col">N° IDENTIFICACION</th>
                    <th scope="col">NOMBRE COMPLETO</th>
                    <th scope="col">PESO ACUMULADO</th>
                    <th scope="col">SOPORTES ASIGNADOS</th>
                    <th scope="col">ACCIONES</th>
                @endslot
                @slot('tbody')
                    @foreach ($trabajadores as $trabajador)
                        <tr>
                            <td scope="row" data-titulo="N°">{{ $loop->iteration }}</td>
                            <td scope="row" data-titulo="N° IDENTIFICACION">{{ $trabajador->identificacion }}</td>
                            <td scope="row" data-titulo="NOMBRE COMPLETO">{{ $trabajador->nombres.' '.$trabajador->apellidos }}</td>
                            <td scope="row" data-titulo="PESO ACUMULADO">
                                @if (count($trabajador->asignaciones) > 0)
                                    {{ $trabajador->pesosAcumulados[0]->peso_acumulado }}
                                @else
                                    0
                                @endif
                            </td>
                            <td scope="row" data-titulo="SOPORTES ASIGNADOS">
                                @if (count($trabajador->asignaciones) > 0)
                                    @foreach ($trabajador->asignaciones as $asignaciones)
                                        <li>{{ $asignaciones->soporte->nombres }}</li>
                                    @endforeach
                                @else
                                    Sin asignar
                                @endif
                            </td>
                            <td scope="row" data-titulo="ACCIONES">
                                @can('trabajadores_actualizar')
                                    <a class="btn btn-soft-secondary btn-border btn-sm m-1" data-bs-toggle="tooltip" data-bs-placement="top" title="Editar"
                                    wire:click="editar({{ $trabajador->id }})">
                                        <i class="ri-edit-box-line"></i>
                                    </a>
                                @endcan
                                <a class="btn btn-soft-secondary btn-border btn-sm m-1" data-bs-toggle="tooltip" data-bs-placement="top" title="Ver"
                                wire:click="verDetalles({{ $trabajador->id }})">
                                    <i class="ri-eye-line"></i>
                                </a>
                            </td>
                        </tr>
                    @endforeach
                @endslot
                <tr>
                    <td>Total registros <b>{{ $trabajadores->total() }}</b></td>
                    <td class="float-end">
                        @if(isset($datosFiltros) && count($datosFiltros) > 0)
                            {{ $trabajadores->appends($datosFiltros)->links() }}
                        @else
                            {{ $trabajadores->links() }}
                        @endif
                    </td>
                </tr>
            @endcomponent
        @endcomponent
    </div>
</div>
