@can('trabajadores_actualizar')
    @component('components.modal', [
            'title' => 'Actualizar Trabajador',
            'id' => 'modalActualizarTrabajador',
            'static' => true,
            'self' => true,
            'size' => 'modal-lg'
        ])
        @slot('body')
            <div class="row">
                <div class="form-group">
                    <label for="nombres" class="form-label">Nombres <span class="text-danger">*</span></label>
                    <input id="nombres" wire:model="nombres" type="text" class="form-control rounded-pill {{ $errors->has('nombres') ? 'is-invalid' : '' }}">

                    @error('nombres')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>

                <div class="form-group">
                    <label for="apellidos" class="form-label">Apellidos <span class="text-danger">*</span></label>
                    <input id="apellidos" wire:model="apellidos" type="text" class="form-control rounded-pill {{ $errors->has('apellidos') ? 'is-invalid' : '' }}">

                    @error('apellidos')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>

                <div class="form-group">
                    <label for="identificacion" class="form-label">Identificación <span class="text-danger">*</span></label>
                    <input id="identificacion" wire:model="identificacion" type="text" class="form-control rounded-pill {{ $errors->has('identificacion') ? 'is-invalid' : '' }}">

                    @error('identificacion')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>

                <div class="form-group">
                    <label for="celular" class="form-label">Celular</label>
                    <input id="celular" wire:model="celular" type="text" class="form-control rounded-pill {{ $errors->has('celular') ? 'is-invalid' : '' }}">

                    @error('celular')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
            </div>
        @endslot
        @slot('footer')
            @component('components.button', [
                'color' => 'secondary',
                'icono' => 'ri-close-circle-line',
                'titulo' => 'Cancelar'
            ])
                data-bs-dismiss="modal"
                wire:click.prevent="cancelar()"
            @endcomponent

            @component('components.button', [
                'color' => 'success',
                'icono' => 'ri-save-line',
                'titulo' => 'Guardar'
            ])
                wire:loading.attr="disabled" wire:click.prevent="actualizar()"
            @endcomponent
        @endslot
    @endcomponent
@endcan
