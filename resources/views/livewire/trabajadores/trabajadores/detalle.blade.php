@component('components.modal', [
        'title' => 'Detalle del Trabajador',
        'id' => 'modalDetalleTrabajador',
        'self' => true,
        'btnCerrar' => true
    ])
    @slot('body')
        @if ($datosTrabajador)
            <div class="mt-2 mb-2">
                <h3 class="text-center texto-subrayado">{{ $datosTrabajador->nombres.' '.$datosTrabajador->apellidos }}</h3>
            </div>

            <div>
                <h5 class="mb-3">Información:</h5>
                <div class="ms-2">
                    <div class="d-flex">
                        <strong class="text-dark col-6">N° Identificación:</strong>
                        <p class="col-6 text-muted">{{ $datosTrabajador->identificacion }}</p>
                    </div>

                    <div class="d-flex">
                        <strong class="text-dark col-6">Celular:</strong>
                        <p class="col-6 text-muted">{{ $datosTrabajador->celular }}</p>
                    </div>

                    @if ($datosTrabajador->usuario)
                        <div class="d-flex">
                            <strong class="text-dark col-6">Email:</strong>
                            <p class="col-6 text-muted">{{ $datosTrabajador->usuario->email }}</p>
                        </div>
                    @endif
                </div>
            </div>
        @endif
    @endslot
@endcomponent
