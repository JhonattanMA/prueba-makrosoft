<div>
    <!-- Alertas -->
    @include('layouts.alerts')

    @can('permisos_registrar')
        @component('components.button', [
            'color' => 'success',
            'icono' => 'ri-add-circle-line',
            'titulo' => 'Registrar'
        ])
            data-bs-toggle="modal" data-bs-target="#modalRegistrarPermiso"
        @endcomponent
    @endcan

    @include('livewire.configuracion.permisos.crear')
    @include('livewire.configuracion.permisos.editar')

    <div class="mt-3">
        @component('components.card')
            @component('components.table')
                @slot('thead')
                    <th scope="col">N°</th>
                    <th scope="col">NOMBRE</th>
                    <th scope="col">ACCIONES</th>
                @endslot
                @slot('tbody')
                    @foreach ($permisos as $permiso)
                        <tr>
                            <td scope="row" data-titulo="N°">{{ $loop->iteration }}</td>
                            <td scope="row" data-titulo="NOMBRE">{{ $permiso->name }}</td>
                            <td scope="row" class="py-0" data-titulo="ACCIONES">
                                @can('permisos_actualizar')
                                    <a class="btn btn-soft-secondary btn-border btn-sm m-1" data-bs-toggle="tooltip" data-bs-placement="top" title="Editar"
                                    wire:click="editar({{ $permiso->id }})">
                                        <i class="ri-edit-box-line"></i>
                                    </a>
                                @endcan
                            </td>
                        </tr>
                    @endforeach
                @endslot
                <tr>
                    <td>Total registros <b>{{ $permisos->total() }}</b></td>
                    <td class="float-end">
                        @if(isset($datosFiltros) && count($datosFiltros) > 0)
                            {{ $permisos->appends($datosFiltros)->links() }}
                        @else
                            {{ $permisos->links() }}
                        @endif
                    </td>
                </tr>
            @endcomponent
        @endcomponent
    </div>
</div>

