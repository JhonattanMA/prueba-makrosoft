@can('usuarios_registrar')
    @component('components.modal', [
            'title' => 'Registrar Usuario',
            'id' => 'modalRegistrarUsuario',
            'static' => true,
            'self' => true
        ])
        @slot('body')
            <div class="form-group">
                <label for="nombre_completo" class="form-label">Nombre Completo <span class="text-danger">*</span></label>
                <input type="text" class="form-control @error('nombre_completo') is-invalid @enderror" wire:model="nombre_completo" id="nombre_completo" placeholder="Ingrese el nombre completo" required>

                @error('nombre_completo')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>

            <div class="form-group">
                <label for="email" class="form-label">Email <span class="text-danger">*</span></label>
                <input type="email" class="form-control @error('email') is-invalid @enderror" wire:model="email" id="email" placeholder="Ingrese el email" required>

                @error('email')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>

            <div class="form-group">
                <label for="password" class="form-label">Contraseña <span class="text-danger">*</span></label>
                <input type="password" class="form-control @error('password') is-invalid @enderror" wire:model="password" id="password" placeholder="Ingrese la contraseña" required>

                @error('password')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>

            <div class=" form-group">
                <label for="input-password">Confirmar Contraseña</label>
                <input type="password" class="form-control @error('password_confirmation') is-invalid @enderror" wire:model="password_confirmation" id="input-password" placeholder="Confirme la contraseña" required>
            </div>
        @endslot
        @slot('footer')
            @component('components.button', [
                'color' => 'secondary',
                'icono' => 'ri-close-circle-line',
                'titulo' => 'Cancelar'
            ])
                data-bs-dismiss="modal"
                wire:click.prevent="cancelar()"
            @endcomponent

            @component('components.button', [
                'color' => 'success',
                'icono' => 'ri-save-line',
                'titulo' => 'Guardar'
            ])
                wire:loading.attr="disabled" wire:click.prevent="registrar()"
            @endcomponent
        @endslot
    @endcomponent
@endcan
