@component('components.modal', [
        'title' => ucfirst($asignarRemover).' Rol',
        'id' => 'modalAsignarRemoverRol',
        'static' => true,
        'self' => true,
        'btnCerrar' => true
    ])
    @slot('body')
        <div class="form-group">
            <label for="rol" class="form-label">Rol:</label>
            <select wire:model="rol" id="rol" class="form-control selectpicker {{ $errors->has('rol') ? 'is-invalid' : '' }} selects selectIgnore" data-live-search="true" title="Seleccione..." data-size="7">
                <option value="">NINGUNO</option>
                @foreach ($roles as $rol)
                    <option value="{{$rol->id}}">{{$rol->name}}</option>
                @endforeach
            </select>

        </div>
        @error('rol')
            <span class="text-sm text-danger" role="alert">
                <strong>{{ $message }}</strong>
            </span>
        @enderror
    @endslot
    @slot('footer')
        @component('components.button', [
            'color' => 'secondary',
            'icono' => 'ri-close-circle-line',
            'titulo' => 'Cancelar'
        ])
            data-bs-dismiss="modal"
            wire:click.prevent="cancelar()"
        @endcomponent
        @if ($asignarRemover == 'asignar')
            @component('components.button', [
                'color' => 'success',
                'icono' => 'ri-save-line',
                'titulo' => 'Guardar'
            ])
                wire:loading.attr="disabled" wire:click.prevent="asignarRolAUsuario()"
            @endcomponent
        @else
            @component('components.button', [
                'color' => 'success',
                'icono' => 'ri-save-line',
                'titulo' => 'Guardar'
            ])
                wire:loading.attr="disabled" wire:click.prevent="removerRolAUsuario()"
            @endcomponent
        @endif

    @endslot
@endcomponent
