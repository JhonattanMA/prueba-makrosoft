@can('usuarios_asignar_permisos')
    @component('components.modal', [
            'title' => 'Asignar Permisos',
            'id' => 'modalAsignarPermisos',
            'static' => true,
            'self' => true,
            'btnCerrar' => true
        ])
        @slot('body')
            <div class="form-group">
                <label for="permisos" class="form-label">Permisos:</label>
                <select wire:model="permisos" id="permisos" class="form-control selectpicker {{ $errors->has('permisos') ? 'is-invalid' : '' }} selectIgnore" multiple data-size="7" data-live-search="true" data-actions-box="true" data-selected-text-format="count" title="Seleccione...">
                    @foreach ($datosPermisos as $permiso)
                        <option value="{{$permiso->id}}">{{$permiso->name}}</option>
                    @endforeach
                </select>
            </div>
            @error('permisos')
                <span class="text-sm text-danger" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        @endslot
        @slot('footer')
            @component('components.button', [
                'color' => 'secondary',
                'icono' => 'ri-close-circle-line',
                'titulo' => 'Cancelar'
            ])
                data-bs-dismiss="modal"
                wire:click.prevent="cancelar()"
            @endcomponent

            @component('components.button', [
                'color' => 'success',
                'icono' => 'ri-save-line',
                'titulo' => 'Guardar'
            ])
                wire:loading.attr="disabled" wire:click.prevent="asignarPermisos()"
            @endcomponent
        @endslot
    @endcomponent
@endcan
