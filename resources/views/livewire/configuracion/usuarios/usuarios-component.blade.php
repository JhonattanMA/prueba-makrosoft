<div>
    <!-- Alertas -->
    @include('layouts.alerts')

    @can('usuarios_registrar')
        @component('components.button', [
            'color' => 'success',
            'icono' => 'ri-add-circle-line',
            'titulo' => 'Registrar'
        ])
            data-bs-toggle="modal" data-bs-target="#modalRegistrarUsuario"
        @endcomponent
    @endcan

    @include('livewire.configuracion.usuarios.crear')
    @include('livewire.configuracion.usuarios.editar')
    @include('livewire.configuracion.usuarios.asignar-permisos')
    @include('livewire.configuracion.usuarios.asignar-remover-rol')

    <div class="mt-3">
        @component('components.card')
            @component('components.table')
                @slot('thead')
                    <th scope="col">N°</th>
                    <th scope="col">NOMBRE USUARIO</th>
                    <th scope="col">ROLES</th>
                    <th scope="col">PERMISOS</th>
                    <th scope="col">ACCIONES</th>
                @endslot
                @slot('tbody')
                    @foreach ($usuarios as $usuario)
                        <tr>
                            <td scope="row" data-titulo="N°">{{ $loop->iteration }}</td>
                            <td scope="row" data-titulo="NOMBRE USUARIO">
                                <div class="d-flex align-items-center pagi-list">
                                    <div class="flex-shrink-0 me-3">
                                        <div>
                                            <img class="image avatar-xs rounded-circle" alt="" src="{{ URL::asset('assets/images/users/admin.jpg') }}">
                                        </div>
                                    </div>
                                    <div class="flex-grow-1 overflow-hidden">
                                        <h5 class="fs-13 mb-1">{{ $usuario->nombre_completo }}</h5>
                                        {{-- <p class="born timestamp text-muted mb-0">Front end Developer</p> --}}
                                    </div>
                                </div>
                            </td>
                            <td scope="row" data-titulo="ROLES">{{str_replace('[','',str_replace(']','',$usuario->getRoleNames()))}}</td>
                            <td scope="row" data-titulo="PERMISOS">
                                <div class="d-flex justify-content-around">
                                    <div>
                                        @foreach ($usuario->getDirectPermissions() as $key => $permiso)
                                            <li>{{$permiso->name}}</li>
                                            @if ($key == 14 || $key == 29)
                                                </div>
                                                <div>
                                            @endif
                                        @endforeach
                                    </div>
                                </div>
                            </td>
                            <td scope="row" class="py-0" data-titulo="ACCIONES">
                                @can('usuarios_actualizar')
                                    <a class="btn btn-soft-secondary btn-border btn-sm m-1" data-bs-toggle="tooltip" data-bs-placement="top" title="Actualizar Usuario"
                                    wire:click="editar({{ $usuario->id }})">
                                        <i class="ri-user-settings-line"></i>
                                    </a>
                                @endcan

                                @can('usuarios_asignar_rol')
                                    <a class="btn btn-soft-secondary btn-border btn-sm m-1" data-bs-toggle="tooltip" data-bs-placement="top" title="Asignar Rol"
                                    wire:click="obtenerIdUsuario({{ $usuario->id }}, 'asignar')">
                                        <i class="ri-user-follow-line"></i>
                                    </a>
                                @endcan

                                @can('usuarios_asignar_permisos')
                                    <a class="btn btn-soft-secondary btn-border btn-sm m-1" data-bs-toggle="tooltip" data-bs-placement="top" title="Asignar Permisos"
                                    wire:click="obtenerIdUsuario({{ $usuario->id }})">
                                        <i class="ri-lock-unlock-line"></i>
                                    </a>
                                @endcan

                                @can('usuarios_remover_rol')
                                    <a class="btn btn-soft-secondary btn-border btn-sm m-1" data-bs-toggle="tooltip" data-bs-placement="top" title="Remover Rol"
                                    wire:click="obtenerIdUsuario({{ $usuario->id }}, 'remover')">
                                        <i class="ri-user-unfollow-line"></i>
                                    </a>
                                @endcan
                            </td>
                        </tr>
                    @endforeach
                @endslot
                <tr>
                    <td>Total registros <b>{{ $usuarios->total() }}</b></td>
                    <td class="float-end">
                        @if(isset($datosFiltros) && count($datosFiltros) > 0)
                            {{ $usuarios->appends($datosFiltros)->links() }}
                        @else
                            {{ $usuarios->links() }}
                        @endif
                    </td>
                </tr>
            @endcomponent
        @endcomponent
    </div>
</div>
