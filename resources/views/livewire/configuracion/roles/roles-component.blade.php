<div>
    <!-- Alertas -->
    @include('layouts.alerts')

    @can('roles_registrar')
        @component('components.button', [
            'color' => 'success',
            'icono' => 'ri-add-circle-line',
            'titulo' => 'Registrar'
        ])
            data-bs-toggle="modal" data-bs-target="#modalRegistrarRol"
        @endcomponent
    @endcan

    @include('livewire.configuracion.roles.crear')
    @include('livewire.configuracion.roles.editar')
    @include('livewire.configuracion.roles.asignar-permisos')

    <div class="mt-3">
        @component('components.card')
            @component('components.table')
                @slot('thead')
                    <th scope="col">N°</th>
                    <th scope="col">NOMBRE</th>
                    <th scope="col">PERMISOS</th>
                    <th scope="col">ACCIONES</th>
                @endslot
                @slot('tbody')
                    @foreach ($roles as $rol)
                        <tr>
                            <td scope="row" data-titulo="N°">{{ $loop->iteration }}</td>
                            <td scope="row" data-titulo="NOMBRE">{{ $rol->name }}</td>
                            <td scope="row" class="py-0" data-titulo="PERMISOS">
                                <div class="d-flex justify-content-around">
                                    <div>
                                        @foreach ($rol->permissions as $key => $permiso)
                                            <li>{{$permiso->name}}</li>
                                            @if ($key == 14 || $key == 29)
                                                </div>
                                                <div>
                                            @endif
                                        @endforeach
                                    </div>
                                </div>
                            </td>
                            <td scope="row" data-titulo="ACCIONES">
                                @can('roles_actualizar')
                                    <a class="btn btn-soft-secondary btn-border btn-sm m-1" data-bs-toggle="tooltip" data-bs-placement="top" title="Editar"
                                    wire:click="editar({{ $rol->id }})">
                                        <i class="ri-edit-box-line"></i>
                                    </a>
                                @endcan

                                @can('roles_asignar_permisos')
                                    <a class="btn btn-soft-secondary btn-border btn-sm m-1" data-bs-toggle="tooltip" data-bs-placement="top" title="Asignar Permisos"
                                    wire:click="obtenerIdRol({{ $rol->id }})">
                                        <i class="ri-lock-unlock-line"></i>
                                    </a>
                                @endcan
                            </td>
                        </tr>
                    @endforeach
                @endslot
                <tr>
                    <td>Total registros <b>{{ $roles->total() }}</b></td>
                    <td class="float-end">
                        @if(isset($datosFiltros) && count($datosFiltros) > 0)
                            {{ $roles->appends($datosFiltros)->links() }}
                        @else
                            {{ $roles->links() }}
                        @endif
                    </td>
                </tr>
            @endcomponent
        @endcomponent
    </div>
</div>
