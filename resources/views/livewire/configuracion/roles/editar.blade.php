@can('roles_actualizar')
    @component('components.modal', [
            'title' => 'Actualizar Rol',
            'id' => 'modalActualizarRol',
            'static' => true,
            'self' => true
        ])
        @slot('body')
            <div class="form-group">
                <label for="nombre" class="form-label">Nombre <span class="text-danger">*</span></label>
                <input id="nombre" wire:model="nombre" type="text" class="form-control rounded-pill {{ $errors->has('nombre') ? 'is-invalid' : '' }}">

                @error('nombre')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>
        @endslot
        @slot('footer')
            @component('components.button', [
                'color' => 'secondary',
                'icono' => 'ri-close-circle-line',
                'titulo' => 'Cancelar'
            ])
                data-bs-dismiss="modal"
                wire:click.prevent="cancelar()"
            @endcomponent

            @component('components.button', [
                'color' => 'success',
                'icono' => 'ri-save-line',
                'titulo' => 'Guardar'
            ])
                wire:loading.attr="disabled" wire:click.prevent="actualizar()"
            @endcomponent
        @endslot
    @endcomponent
@endcan
