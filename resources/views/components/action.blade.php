<div>
	<a href="#" class="btn btn-rojo btn-sm" role="button" data-bs-toggle="dropdown" data-bs-boundary="window" aria-expanded="false">
        <i class="ri-list-settings-line"></i>
    </a>
	<div class="dropdown-menu p-1">
		{{ $slot }}
	</div>
</div>
