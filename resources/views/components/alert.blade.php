<div class="col-11 mx-auto" id="notificacion">
    <div class="alert alert-{{isset($tipo) ? (($tipo != 'error') ? $tipo : 'danger') : ''}} alert-border-left alert-dismissible fade show" role="alert" style="font-size: 16px">
        @if ($tipo == 'success')
            <i class="ri-checkbox-circle-line me-3 align-middle"></i>
        @elseif ($tipo == 'info')
            <i class="ri-information-line  me-3 align-middle"></i>
        @elseif ($tipo == 'warning')
            <i class="ri-spam-2-line me-3 align-middle"></i>
        @elseif ($tipo == 'error')
            <i class="ri-close-circle-line me-3 align-middle"></i>
        @endif
        {{ session(isset($tipo) ? $tipo : '') }}
        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
    </div>
</div>
