<div class="modal zoomIn" {{isset($self) ? 'wire:ignore.self' : ''}} id="{{ $id }}" {{ isset($static) ? 'data-bs-backdrop="static"' : '' }} tabindex="-1" aria-hidden="true">
    <div class="modal-dialog {{ isset($size) ? $size : '' }}">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title text-rojo">{{ $title }}</h5>
                @if(isset($btnCerrar))
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"> </button>
                @endif
            </div>
            <div class="modal-body">
                {{ $body }}
            </div>
            @if (isset($footer))
                <div class="modal-footer d-flex justify-content-around">
                    @if(isset($btnCancel))
                        @component('components.button', [
                            'color' => 'secondary',
                            'icono' => 'ri-close-circle-line',
                            'titulo' => 'Cerrar'
                        ])
                            data-bs-dismiss="modal"
                        @endcomponent

                    @endif
                    {{ isset($footer) ? $footer : '' }}
                </div>
            @endif
        </div>
    </div>
</div>
