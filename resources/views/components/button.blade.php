<button type="button" class="btn btn-{{ $color }} btn-label waves-effect waves-light {{ isset($clases) ? $clases : '' }}" {{ $slot }}
style="font-size: 13px" wire:click="{{ isset($wireClick) ? $wireClick : '' }}">
    <i class="{{ $icono }} label-icon align-middle fs-20 me-2"></i> {{ $titulo }}
</button>
