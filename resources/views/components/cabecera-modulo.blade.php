<div class="profile-foreground position-relative mx-n4 mt-n4">
    <div class="profile-wid-bg">
        <img src="{{ $imagen }}" alt="" class="profile-wid-img" />
    </div>
</div>
<div class="pt-4 mb-4 mb-lg-3 pb-lg-4">
    <div class="row text text-white-50 mt-5 px-3 text-justify">
        <div class="alert alert-dismissible alert-label-icon label-arrow fade show mt-5 text-white" role="alert">
            <i class="ri-information-line label-icon"></i><strong>{{ $mensaje }}</strong>
        </div>
    </div>
</div>
