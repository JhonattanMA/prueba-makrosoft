<div class="card mb-3 {{ (isset($shadow) ? 'shadow-sm' : '') }} {{ (isset($style) ? $style : '') }}">
	@isset($header)
		<div class="card-header p-2 {{  isset($clases) ? $clases : ''  }}">
    		{{ $header }}
  		</div>
  	@endisset

    @isset($img)
        {{ $img }}
    @endisset
	<div class="card-body p-2">
		@isset($title)
  			<h4 class="card-title mb-2">{{ $title }}</h4>
  		@endisset
		<div class="mt-3">{{ $slot }}</div>
	</div>
</div>
