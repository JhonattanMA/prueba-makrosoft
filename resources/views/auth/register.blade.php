@extends('layouts.master-without-nav')
@section('title')
    {{-- @lang('translation.signup') --}}
    Registrarse
@endsection
@section('content')
    <!-- auth-page wrapper -->
    <div class="auth-page-wrapper auth-bg-cover py-5 d-flex justify-content-center align-items-center min-vh-100">
        <div class="bg-overlay"></div>
        <!-- auth-page content -->
        <div class="auth-page-content overflow-hidden pt-lg-5">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card overflow-hidden m-0">
                            <div class="row justify-content-center g-0">
                                <div class="col-lg-6">
                                    <div class="p-lg-5 p-4 auth-one-bg h-100">
                                        <div class="position-relative h-100 d-flex flex-column">
                                            <div class="mb-4">
                                                <a href="{{ route('login') }}" class="d-block">
                                                    <img src="{{ URL::asset('assets/images/logo-light.png') }}" alt="sus-lentes-logo" width="160">
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-6">
                                    <div class="p-lg-5 p-4">
                                        <div class="text-center">
                                            <h5 class="text-rojo">Crear una nueva cuenta</h5>
                                            <p class="text-muted">Registrate ahora</p>
                                        </div>

                                        <div class="mt-4">
                                            <form class="needs-validation" novalidate method="POST" action="{{ route('register') }}" enctype="multipart/form-data">
                                                @csrf
                                                <div class="mb-3">
                                                    <label for="nombres" class="form-label">Nombres <span class="text-danger">*</span></label>
                                                    <input type="text" class="form-control @error('nombres') is-invalid @enderror" name="nombres" value="{{ old('nombres') }}" id="nombres" placeholder="Ingrese los nombres" required>

                                                    @error('nombres')
                                                        <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $message }}</strong>
                                                        </span>
                                                    @enderror
                                                </div>

                                                <div class="mb-3">
                                                    <label for="apellidos" class="form-label">Apellidos <span class="text-danger">*</span></label>
                                                    <input type="text" class="form-control @error('apellidos') is-invalid @enderror" name="apellidos" value="{{ old('apellidos') }}" id="apellidos" placeholder="Ingrese los apellidos" required>

                                                    @error('apellidos')
                                                        <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $message }}</strong>
                                                        </span>
                                                    @enderror
                                                </div>

                                                <div class="mb-3">
                                                    <label for="identificacion" class="form-label">N° Identificación <span class="text-danger">*</span></label>
                                                    <input type="text" class="form-control @error('identificacion') is-invalid @enderror" name="identificacion" value="{{ old('identificacion') }}" id="identificacion" placeholder="Ingrese la identificacion" required>

                                                    @error('identificacion')
                                                        <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $message }}</strong>
                                                        </span>
                                                    @enderror
                                                </div>

                                                <div class="mb-3">
                                                    <label for="celular" class="form-label">N° Celular</label>
                                                    <input type="text" class="form-control @error('celular') is-invalid @enderror" name="celular" value="{{ old('celular') }}" id="celular" placeholder="Ingrese el celular" required>

                                                    @error('celular')
                                                        <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $message }}</strong>
                                                        </span>
                                                    @enderror
                                                </div>

                                                <div class="mb-3">
                                                    <label for="useremail" class="form-label">Email <span class="text-danger">*</span></label>
                                                    <input type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" id="useremail" placeholder="Ingrese el email" required>

                                                    @error('email')
                                                        <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $message }}</strong>
                                                        </span>
                                                    @enderror
                                                </div>

                                                <div class="mb-2">
                                                    <label for="userpassword" class="form-label">Contraseña <span class="text-danger">*</span></label>
                                                    <input type="password" class="form-control @error('password') is-invalid @enderror" name="password" id="userpassword" placeholder="Ingrese la contraseña" required>

                                                    @error('password')
                                                        <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $message }}</strong>
                                                        </span>
                                                    @enderror
                                                </div>

                                                <div class=" mb-2">
                                                    <label for="input-password">Confirmar Contraseña</label>
                                                    <input type="password" class="form-control @error('password_confirmation') is-invalid @enderror" name="password_confirmation" id="input-password" placeholder="Confirme la contraseña" required>

                                                    <div class="form-floating-icon">
                                                        <i data-feather="lock"></i>
                                                    </div>
                                                </div>

                                                {{-- <div class="mb-4">
                                                    <p class="mb-0 fs-12 text-muted fst-italic">
                                                        Al registrarse aceptas los
                                                        <a href="#" class="text-primary text-decoration-underline fst-normal fw-medium">
                                                            Terminos de uso
                                                        </a>
                                                    </p>
                                                </div> --}}

                                                <div class="mt-4">
                                                    <button class="btn btn-rojo w-100" type="submit">Registrarse</button>
                                                </div>
                                            </form>
                                        </div>

                                        <div class="mt-5 text-center">
                                            <p class="mb-0">
                                                ¿Ya tienes una cuenta?
                                                <a href="{{ route('login') }}" class="fw-semibold text-primary text-decoration-underline"> Iniciar Sesión</a>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- end card -->
                    </div>
                    <!-- end col -->

                </div>
                <!-- end row -->
            </div>
            <!-- end container -->
        </div>
        <!-- end auth page content -->

        <!-- footer -->
        <footer class="footer">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="text-center">
                            <p class="text-dark">
                                Copyrights &copy;
                                <script>document.write(new Date().getFullYear())</script>
                                Makrosoft.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
        <!-- end Footer -->
    </div>
    <!-- end auth-page-wrapper -->
@endsection
@section('script')
    <script src="{{ URL::asset('assets/js/pages/form-validation.init.js') }}"></script>
    <script src="{{ URL::asset('assets/js/pages/passowrd-create.init.js') }}"></script>
@endsection
